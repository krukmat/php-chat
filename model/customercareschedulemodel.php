<?php

namespace Model;

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE); //TODO: Por mensaje de condicion estricta en search


/**
 * Description of customercareschedulemodel
 */
class CustomerCareScheduleModel extends \Core\Model
{

    /**
     * Create new model
     */
    function __construct()
    {
        $this->idName = 'token';
        $this->modelName = 'customercareschedule';
    }



    public function search($filter, $only_one = true)
    {
        return parent::search('CustomerCareScheduleEntity', $filter, $only_one);
    }

    private function checkTimeOrder($time_noon_from, $time_noon_to, $time_night_from, $time_night_to, &$customerCareScheduleEntity)
    {
        //Noon
        $invalid_pair_from_to = !($customerCareScheduleEntity->updateTimeField('time_noon_from','time_noon_to', $time_noon_from, $time_noon_to));
        
        //Night
        $invalid_pair_from_to = $invalid_pair_from_to || !($customerCareScheduleEntity->updateTimeField('time_night_from', 'time_night_to', 
            $time_night_from, $time_night_to));

        return $invalid_pair_from_to;
    }

	
    /**
     * @return \Entities\CustomerCareScheduleEntity
     */
    public function createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, 
        $time_noon_to, $time_night_from, $time_night_to)
    {
        $customerCareScheduleEntity = new \Entities\CustomerCareScheduleEntity();
        $customerCareScheduleEntity->token = $this->createToken();
        // chequeo que date_from < date_to
        try
        {
            $customerCareScheduleEntity->assignDates($date_from, $date_to);
        }
        catch (\Exception $e) {
            return null;
        }    
        $customerCareScheduleEntity->week_days = $week_days;

        $invalid_pair_from_to = $this->checkTimeOrder($time_noon_from, $time_noon_to, $time_night_from, $time_night_to, $customerCareScheduleEntity);
        
        if (!$invalid_pair_from_to)
        {
            $this->saveEntity($customerCareScheduleEntity);
            return $customerCareScheduleEntity;
        }

        return null;
        
    }

    /**
     * @return \Entities\CustomerCareScheduleEntity
     */
    public function updateCustomerCaresSchedule($token, $date_from, $date_to, $week_days, $time_noon_from, 
        $time_noon_to, $time_night_from, $time_night_to)
    {

        $filter = array("token" => $token);
        $customerCareScheduleEntity = $this->search($filter);

        if ($customerCareScheduleEntity) 
        {   
            try
            {
                 $customerCareScheduleEntity->assignDates($date_from, $date_to);
            }
            catch (\Exception $e) {
                return null;
            }
           
            $customerCareScheduleEntity->week_days = $week_days;

            $invalid_pair_from_to = $this->checkTimeOrder($time_noon_from, $time_noon_to, $time_night_from, $time_night_to, $customerCareScheduleEntity);
        
            if (!$invalid_pair_from_to)
            {
                $this->saveEntity($customerCareScheduleEntity);
                return $customerCareScheduleEntity;
            }

            return null;

        }

        return null;
    }


    /**
     * @return true if current datetime is in interval -> date or weekday and hour range. Otherwise should
       @return false
     */

    public function isServiceAvailable($current_date) 
    {   
        
        $current_hour = date('H:i', strtotime($current_date));
        $current_date = date('Y-m-d', strtotime($current_date));
        $mongo_current_date = new \MongoDate(strtotime($current_date));

         // buscando fecha especifica al dia. Mejorar esto. Debe ser date_from === date_to
        $filter = array("date_from" => array('$lte' => $mongo_current_date), "date_to" => array('$gte' => $mongo_current_date));
        $entity = $this->search($filter);

        if ($entity) 
        {
            return $entity->checkTimeInInterval($current_hour);
        }

        // si no hay fecha => tomo el que corresponde al dia de la semana
        // 0..6 weekdays
        $dayofweek = date('w', strtotime($current_date));
        $filter = array("week_days" => intval($dayofweek));

        $entity = $this->search($filter);

        if ($entity) 
        {
            return $entity->checkTimeInInterval($current_hour);
        } else {
            return false;
        }
    }

    private function getDatePeriod(&$date_start, &$date_end)
    {
        $interval = date_diff($date_start, $date_end);
        $int_interval = intval($interval->format('%R%a'));
        if ($int_interval < 1)
        {   
            $period = [$date_start];

        } else {
            // intervalo de 1 dia
            $interval = \DateInterval::createFromDateString('1 day');

            // agrego un dia mas para incluir la fecha de fin
            $date_end = $date_end->modify( '+1 day' ); 

            //var_dump(get_defined_vars());
            $period = new \DatePeriod($date_start, $interval, $date_end);
        }
        return $period;
    }

    /**
    * @return array with events given the interval
    **/

    public function getScheduleTable ($date_start, $date_end) 
    {   
        // Obtengo todas las fechas especificadas en el periodo
        $period = $this->getDatePeriod($date_start, $date_end);

        $schedule_table = [];

        foreach ( $period as $dt )
        {
            $current_date = $dt->format( "Y-m-d" );
            // chequeo si hay horario especial en esa fecha. sino tomo el weekday
            $mongo_current_date = new \MongoDate(strtotime($current_date));

             // buscando fecha especifica al dia. no contempla intervalos. porlo especificado no es necesario
           $filter = array("date_from" => array('$lte' => $mongo_current_date), "date_to" => array('$gte' => $mongo_current_date));
            $entity = $this->search($filter);

            if ($entity) 
            {
                $nightTime = $entity->returnNight($current_date);
                $noonTime = $entity->returnNoon($current_date);
                $schedule_table[] = array('time_night' => $nightTime, 'time_noon' => $noonTime, 'token' => $entity->token); 

            }
        }

        return $schedule_table;
    }


    /**
    * @return array with standard events given the interval
    **/

    public function getStandardScheduleTable () 
    {   
        // De Domingo:0, a Sabado: 6
        $period = [0, 1, 2, 3, 4, 5, 6];
        $current_date = date('Y-m-d');
        

        $schedule_table = [];

        foreach ( $period as $dt )
        {
             // Quitar esto al devolver schedule table. No es muestra en el calendario las fechas regulares.
             // si no hay fecha => tomo el que corresponde al dia de la semana
            // 0..6 weekdays
            
            $filter = array("week_days" => $dt);
            $entity = $this->search($filter);

            if ($entity) 
            {
                 // chequeo por hora si hay servicio o no
                // desde las 00:00 hasta las 23:00
                $nightTime = $entity->returnNight($current_date);
                $noonTime = $entity->returnNoon($current_date);
                $schedule_table[] = array('time_night' => $nightTime, 'time_noon' => $noonTime, 'token' => $entity->token );
            }
        }

        return $schedule_table;
    }
	
}