<?php
namespace Model;


require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/entity.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';

require_once dirname(__FILE__) .'/configparamsmodel.php';
require_once dirname(__FILE__) .'/../entities/configparamsentity.php';

/**
 * Description of ConfigParamsModelTest
 *
 * @author krukmat
 */
 
class ConfigParamsModelTest extends ModelTest
{
	protected function setUp()
    {	
    	global $instance, $modelName;
    	$instance = ConfigParamsModel::singleton();
    	$modelName = $instance->getModelName();  //It's used for delete the collection aftwerwards
		ModelTest::setUp();
    }

    public function testSave()
    {
    	global $instance;
   
    	$instance->save(1, 'hi there', 'hi there');

    	$this->assertEquals($instance->count(array()), 1);

    	$instance->save(2, 'hi there', 'hi there');

    	$this->assertEquals($instance->count(array()), 1);
    }

}