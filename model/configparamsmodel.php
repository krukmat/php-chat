<?php

namespace Model;

/**
 * Description of ConfigParamsModel: Singleton configuration
 */
final class ConfigParamsModel extends \Core\Model
{
    /**
     * Create new model
     */
    private function __construct()
    {
        $this->idName = 'token';
        $this->modelName = 'configparams';
    }

    public function current()
    {
    	$entity = $this->search('ConfigParamsEntity', array(), true);
    	return $entity;
    }

    public function save($watson_rate, $message_agent_derivation, $message_out_of_line)
    {   
        // Siempre habra un solo registro
    	try
    	{
    		$this->delete(array());
    	}
    	catch (Exception $e) 
    	{

    	}

    	$entity = new \Entities\ConfigParamsEntity();
    	$entity->token = $this->createToken();
    	$entity->watson_rate = $watson_rate;
    	$entity->message_agent_derivation = $message_agent_derivation;
    	$entity->message_out_of_line = $message_out_of_line;

    	$this->saveEntity($entity);
    }

    public static function singleton()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new ConfigParamsModel();
        }
        return $inst;
    }
    // @codeCoverageIgnoreStart
    public static function getMessageAgentDerivation()
    {
        return ConfigParamsModel::singleton()->current()->message_agent_derivation;
    }

    public static function getMessageOutOfLine()
    {
        return ConfigParamsModel::singleton()->current()->message_out_of_line;
    }
    // @codeCoverageIgnoreEnd

}