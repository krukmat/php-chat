<?php

namespace Model;

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE); //TODO: Por mensaje de condicion estricta en search


/**
 * Description of ChatSessionModel
 */
class ChatSessionModel extends \Core\Model
{

    /**
     * Create new model
     */
    function __construct()
    {
        $this->idName = 'token';
        $this->modelName = 'chatsession';
    }

    public function search($filter, $only_one = true)
    {
        return parent::search('ChatSessionEntity', $filter, $only_one);
    }

    function getSessionData($session_id)
    {
        $filter = array("token" => $session_id);

        $entity = $this->search($filter);

        return $entity; 
    }

    /**
     * @return \Entities\ChatSessionEntity
     */
    function updateSessionData($session_id, $session_creds)
    {
        $entity = $this->getSessionData($session_id);
        if (isset($entity))
        {
            $entity->assignValues($session_creds);
            $this->saveEntity($entity);
            return $entity;
        }
        return null;
    }

    function updateChatHistory($session_id, $chat_record) 
    {
        $entity = $this->getSessionData($session_id);
        if (isset($entity))
        {
            $entity->appendToChatHistory($chat_record);
            $this->saveEntity($entity);
            return $entity;
        }
        return null;
    }

    public function watsonRate()
    {
        $configParams = ConfigParamsModel::singleton();
        $configParams = $configParams->current();
        return $configParams->watson_rate;
    }

    /**
     * @return \Entities\ChatSessionEntity
     */
    public function createChatSession($user_name='', $user_email='', $headers = array())
    {	
		$chatSession = new \Entities\ChatSessionEntity();
        $chatSession->token = $this->createToken();
        $chatSession->send_to_ia = $this->sendToIA($this->watsonRate());
        $chatSession->user_name = $user_name;
        $chatSession->user_email = $user_email;
        $chatSession->headers = $headers;
        $this->saveEntity($chatSession);

        return $chatSession;
    }

    public function sendToIA($ratio)
    {

        $filter = array(
            "send_to_ia" => true
        );

        $orderBy = array(
            "created" => -1
        );
        $numItems = 1;
        $page = 0;
        $entities = $this->getListsEntities("ChatSessionEntity", $filter, $numItems, $page, $orderBy);
		
        if (($entities) && (count($entities) > 0)) {

            $filter = array(
                "created" => array(
                    '$gte' => $entities[0]->created
                ),
                "send_to_ia" => false
            );

            $cant = $this->count($filter);
            
            
            if ($cant < $ratio) {
                return false;
            }
        }


        return true;
    }

}
