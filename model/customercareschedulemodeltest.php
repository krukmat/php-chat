<?php
namespace Model;

require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';

require_once dirname(__FILE__) .'./customercareschedulemodel.php';
require_once dirname(__FILE__) .'/../entities/customercarescheduleentity.php';


/**
 * Description of customercareschedulemodel Tests
 *
 * @author krukmat
 */
 
class CustomerCareScheduleModelTest extends ModelTest
{

	protected function setUp()
    {
        global $customerCareScheduleModel, $modelName;
		$customerCareScheduleModel = new \Model\CustomerCareScheduleModel();
		$modelName = $customerCareScheduleModel->getModelName();  //It's used for delete the collection aftwerwards
		ModelTest::setUp();
    }

     /**
	*   createCustomerCaresSchedule: Case when there's date_from and date_to defined. But date_to < date_from, which is invalid 
	*/

	public function testcreateCustomerCaresScheduleInvalidDateorder() 
	{
		

		global $customerCareScheduleModel;

		function convertMongoDate($date_object) 
	    {
	        $date_php = date('Y-m-d', $date_object->sec);
	        return $date_php;
	    }

		$date_from =date('Y-m-d H:i', strtotime(' +4 day'));
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$this->assertNull($entity);
	}

	/**
	*   createCustomerCaresSchedule: Case when there's date_from and date_to defined. but invalid time order 
	*/

	public function testcreateCustomerCaresScheduleInvalidTimeOrder() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '19:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$this->assertNull($entity);
	}

	public function testcreateCustomerCaresScheduleInvalidTimeOrderNight() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '13:00';
		$time_noon_to = '15:00';
		$time_night_from = '21:00';
		$time_night_to = '20:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$this->assertNull($entity);
	}

    /**
	*   createCustomerCaresSchedule: Case when there's date_from and date_to defined. no week days defined. 
	*/

	public function testcreateCustomerCaresScheduleExactDate() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$this->assertInstanceOf('\Entities\CustomerCareScheduleEntity', $entity);

		$this->assertEquals(count($customerCareScheduleModel->getListsEntities('CustomerCareScheduleEntity',[])), 1);
		$record = $customerCareScheduleModel->getListsEntities('CustomerCareScheduleEntity',[])[0];
		$this->assertEquals($record->time_noon_from, '9:00');
		$this->assertEquals($record->time_noon_to, '13:00');
		$this->assertEquals($record->time_night_from, '17:00');
		$this->assertEquals($record->time_night_to, '22:00');
		$this->assertEquals($record->week_days, null);
		$this->assertEquals($record->date_from, date("Y-m-d"));
		$this->assertEquals($record->date_to, date("Y-m-d"));
	}

	/**
	*   createCustomerCaresSchedule: Case when there's no date_from or date_to defined 
	*/

	public function testcreateCustomerCaresScheduleOnlyWeekdays() 
	{
		global $customerCareScheduleModel;

		$date_from = null;
		$date_to = null;
		$week_days = [2,3];
		$time_noon_from = '10:00';
		$time_noon_to = '14:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$this->assertInstanceOf('\Entities\CustomerCareScheduleEntity', $entity);

		$this->assertEquals(count($customerCareScheduleModel->getListsEntities('CustomerCareScheduleEntity',[])), 1);
		$record = $customerCareScheduleModel->getListsEntities('CustomerCareScheduleEntity',[])[0];

		$this->assertNull($record->date_from);
		$this->assertNull($record->date_to);
		$this->assertEquals($record->time_noon_from, '10:00');
		$this->assertEquals($record->time_noon_to, '14:00');
		$this->assertEquals($record->time_night_from, '17:00');
		$this->assertEquals($record->time_night_to, '22:00');
		$this->assertEquals($record->week_days, [2,3]);
	}

	/**
	*  IsServiceAvailable test 
	*/
	public function testIsServiceAvailableWithDateOk() 
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d H:i');

		$date_from = date('Y-m-d');
		$date_to = date('Y-m-d');
		$week_days = [0,1,2,3,4,5,6];
		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, null, null);

		$this->assertTrue($customerCareScheduleModel->isServiceAvailable($current_date)); // fecha puntual. debe retornar true
	}

	public function testIsServiceAvailableWithDayOfWeekOk() 
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d H:i');
		$week_days = [0,1,2,3,4,5,6];
		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule(null, null, $week_days, $time_noon_from, $time_noon_to, null, null);

		$this->assertTrue($customerCareScheduleModel->isServiceAvailable($current_date)); // fecha puntual. debe retornar true
	}

	public function testIsServiceAvailableWithDayOfWeekBad() 
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d H:i');

		$tomorrow = new \DateTime('tomorrow');
		$week_day = $tomorrow->format('w');
		$week_days = [$week_day]; // only available in tomorrow
		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule(null, null, $week_days, $time_noon_from, $time_noon_to, null, null);

		$this->assertFalse($customerCareScheduleModel->isServiceAvailable($current_date)); // fecha puntual. debe retornar true
	}

	public function testIsServiceAvailableWithDateBad() 
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d H:i');

		$tomorrow = new \DateTime('tomorrow');
		$tomorrow = $tomorrow->format('Y-m-d');
		
		$date_from = $tomorrow;
		$date_to = $tomorrow;

		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, null, $time_noon_from, $time_noon_to, null, null);

		$this->assertFalse($customerCareScheduleModel->isServiceAvailable($current_date)); // fecha puntual. debe retornar true
	}

	public function testIsServiceAvailableBetweenInterval() 
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d H:i');

		$tomorrow = new \DateTime('tomorrow');
		$tomorrow = $tomorrow->format('Y-m-d H:i');
		
		$date_from = $current_date;
		$date_to = date('Y-m-d H:i', strtotime(' +4 day'));

		$yesterday = date('Y-m-d H:i', strtotime(' -1 day'));

		$plus_5_days = date('Y-m-d H:i', strtotime(' +5 day'));

		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		//var_dump(get_defined_vars());

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, null, $time_noon_from, $time_noon_to, null, null);

		$this->assertTrue($customerCareScheduleModel->isServiceAvailable($tomorrow)); // fecha puntual. debe retornar true

		$this->assertFalse($customerCareScheduleModel->isServiceAvailable($yesterday)); // ayer. Fuera de intervalo

		$this->assertFalse($customerCareScheduleModel->isServiceAvailable($plus_5_days)); // ayer. Fuera de intervalo
	}

	public function testSearchOneFalse()
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d');
		
		$date_from = $current_date;
		$date_to = $current_date;

		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, null, $time_noon_from, $time_noon_to, null, null);

		$entity = $customerCareScheduleModel->search([], false);
		$this->assertEquals(count($entity), 1);
		$entity = $entity[0];
		$this->assertEquals($entity->date_from, $entity->date_to);
		$this->assertEquals($entity->date_to, date('Y-m-d'));


	}

	public function testSearch()
	{
		global $customerCareScheduleModel;

		$current_date = date('Y-m-d');
		
		$date_from = $current_date;
		$date_to = $current_date;

		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, null, $time_noon_from, $time_noon_to, null, null);

		$entity = $customerCareScheduleModel->search([], true);
		$this->assertEquals($entity->date_from, $entity->date_to);
		$this->assertEquals($entity->date_to, date('Y-m-d'));


	}

	public function testSearchNoDates()
	{
		global $customerCareScheduleModel;

		$date_from = null;
		$date_to = null;

		$time_noon_from = '0:00';
		$time_noon_to = '23:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, null, $time_noon_from, $time_noon_to, null, null);

		$entity = $customerCareScheduleModel->search([], true);
		$this->assertEquals($entity->date_from, null);
		$this->assertEquals($entity->date_to, null);
	}

	public function testGetScheduleTableOk() 
	{
		global $customerCareScheduleModel;


		//horario especial
		$current_date = date('Y-m-d H:i');

		$tomorrow = new \DateTime('tomorrow');
		$tomorrow = $tomorrow->format('Y-m-d H:i');
		
		$date_from = $current_date;
		$date_to = date('Y-m-d H:i', strtotime(' +4 day'));

		$yesterday = date('Y-m-d H:i', strtotime(' -1 day'));

		$plus_5_days = date('Y-m-d H:i', strtotime(' +5 day'));

		$plus_2_days = new \DateTime(date('Y-m-d H:i', strtotime(' +2 day')));
		$plus_3_days = new \DateTime(date('Y-m-d H:i', strtotime(' +3 day')));

		$plus_2_days_noon = new \DateTime(date('Y-m-d 11:i', strtotime(' +2 day')));
		$plus_3_days_noon = new \DateTime(date('Y-m-d 12:i', strtotime(' +3 day')));

		$time_noon_from = '10:00';
		$time_noon_to = '13:00';

		$time_night_from = '15:00';
		$time_night_to = '22:00';


		$customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, null, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		//Creo un horario standard y chequeo que no tome este sino el que corresponde
		$time_noon_from = '10:00';
		$time_noon_to = '11:00';
		$week_days = [0,1,2,3,4,5,6];

		$customerCareScheduleModel->createCustomerCaresSchedule(null, null, $week_days, $time_noon_from, $time_noon_to,null, null);

		$schedule_table = $customerCareScheduleModel->getScheduleTable ($plus_2_days, $plus_3_days); // 2 days

		//debe tomar el horario especial y no el standard
		$this->assertEquals(count($schedule_table), 2); // 2 registros => plus_2_days y plus_3_days

		$schedule_table = $customerCareScheduleModel->getScheduleTable ($plus_2_days_noon, $plus_3_days_noon); // 2 days

		$this->assertEquals(count($schedule_table), 3); // 2 registros => plus_2_days y plus_3_days

		// 2 days
		$plus_4_days = new \DateTime(date('Y-m-d H:i', strtotime(' +4 day')));
		$schedule_table = $customerCareScheduleModel->getScheduleTable ($plus_2_days, $plus_4_days); // 3 days

		//debe tomar el horario especial y no el standard
		$this->assertEquals(count($schedule_table), 3); // $plus_2_days, $plus_3_days ,$plus_4_days


		// Chequeo un dia fuera del intervalo especial => Al ser standard no lo devuelvo
		$plus_5_days = new \DateTime(date('Y-m-d H:i', strtotime(' +5 day')));
		$schedule_table = $customerCareScheduleModel->getScheduleTable ($plus_5_days, $plus_5_days);
		$this->assertEquals(count($schedule_table), 0);
	}

	public function testGetScheduleTableBad() 
	{
		global $customerCareScheduleModel;

		$current_date = new \DateTime(date('Y-m-d H:i'));

		$tomorrow = new \DateTime('tomorrow');
		
		$date_from = $current_date;
		$plus_4_days = date('Y-m-d H:i', strtotime(' +4 day'));

		$yesterday = date('Y-m-d H:i', strtotime(' -1 day'));

		$plus_2_days = date('Y-m-d H:i', strtotime(' +2 day'));

		$plus_5_days = date('Y-m-d H:i', strtotime(' +5 day'));

		
		$plus_3_days = new \DateTime(date('Y-m-d H:i', strtotime(' +3 day')));

		$time_noon_from = '10:00';
		$time_noon_to = '13:00';

		$time_night_from = '15:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($plus_2_days, $plus_4_days, null, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$schedule_table = $customerCareScheduleModel->getScheduleTable ($current_date, $tomorrow); 

		$this->assertEquals(count($schedule_table), 0); // It should return nothing
	}

	public function testGetStandardTableOk()
	{
		global $customerCareScheduleModel;
		$week_days = [0,1,2,3,4,5,6];

		foreach ($week_days as $week_day)
		{
			$time_noon_from = '0:00';
			$time_noon_to = '23:00';

			$entity = $customerCareScheduleModel->createCustomerCaresSchedule(null, null, [$week_day], $time_noon_from, $time_noon_to, null, null);
		}

		$standard_table = $customerCareScheduleModel->getStandardScheduleTable();
		$this->assertEquals(count($standard_table), 7);
	}

	public function testGetStandardTableOnlyNightOk()
	{
		global $customerCareScheduleModel;
		$week_days = [0,1,2,3,4,5,6];

		foreach ($week_days as $week_day)
		{
			$time_night_from = '21:00';
			$time_night_to = '23:00';

			$entity = $customerCareScheduleModel->createCustomerCaresSchedule(null, null, [$week_day], null, null, $time_night_from, $time_night_to);
		}

		$standard_table = $customerCareScheduleModel->getStandardScheduleTable();
		$this->assertEquals(count($standard_table), 7);
	}

	public function testDelete()
	{
		global $customerCareScheduleModel;
		$week_days = [0,1,2,3,4,5,6];

		foreach ($week_days as $week_day)
		{
			$time_noon_from = '0:00';
			$time_noon_to = '23:00';

			$entity = $customerCareScheduleModel->createCustomerCaresSchedule(null, null, [$week_day], $time_noon_from, $time_noon_to, null, null);
		}

		$filter = array();

		$count = $customerCareScheduleModel->count($filter);
		$this->assertEquals($count, 7);

		//delete elements
		foreach ($week_days as $week_day)
		{	
			$filter = array("week_days" => $week_day);
			$customerCareScheduleModel->delete($filter);
		}

		$count = $customerCareScheduleModel->count($filter);
		$this->assertEquals($count, 0);
	}

	/**
	*   updateCustomerCaresSchedule: Case when there's date_from and date_to defined. no week days defined. 
	*/

	public function testUpdateCustomerCaresScheduleExactDate() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$token = $entity->token;
		$time_night_to = '23:00';

		$entity = $customerCareScheduleModel->updateCustomerCaresSchedule($token, $date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);
		
		$this->assertEquals($entity->token, $token);
		
	}

	/**
	*   updateCustomerCaresSchedule: Case when there's date_from and date_to defined. no week days defined. 
	*/

	public function testUpdateCustomerCaresScheduleInvalidDateOrder() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$token = $entity->token;
		$date_from = date('Y-m-d H:i', strtotime(' +4 day'));

		$entity = $customerCareScheduleModel->updateCustomerCaresSchedule($token, $date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);
		
		$this->assertNull($entity);
		
	}

	public function testUpdateCustomerCaresScheduleInvalidTimeOrder() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$token = $entity->token;
		$time_night_to = '14:00';

		$entity = $customerCareScheduleModel->updateCustomerCaresSchedule($token, $date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);
		
		$this->assertNull($entity);
		
	}

	public function testUpdateCustomerCaresScheduleInvalidToken() 
	{
		

		global $customerCareScheduleModel;

		$date_from = date("Y-m-d");
		$date_to = date("Y-m-d");
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$token = 'fake';

		$entity = $customerCareScheduleModel->updateCustomerCaresSchedule($token, $date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);
		
		$this->assertNull($entity);
		
	}

}