<?php
namespace Model;


require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/entity.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';

require_once dirname(__FILE__) .'/chatsessionmodel.php';
require_once dirname(__FILE__) .'/../entities/chatsessionentity.php';

/**
 * Description of ChatSessionModel Tests
 *
 * @author krukmat
 */
 
class ChatSessionModelTest extends ModelTest
{
	protected function setUp()
    {
        global $chatSessionModel, $modelName;
		$chatSessionModel = new \Model\ChatSessionModel();
		$modelName = $chatSessionModel->getModelName();  //It's used for delete the collection aftwerwards
		ModelTest::setUp();
    }
	
	public function testSendToIATrueWhenInit()
	{	
		//there are no session so it should return true
		$mockCoreModel = $this->getMock('Model\ChatSessionModel', array('getListsEntities'));
		$mockCoreModel->expects($this->any())->method('getListsEntities')->will($this->returnValue([]));
		// It should return true
		$this->assertEquals($mockCoreModel->sendToIA(4), true);
	}
	
	public function testSendToIAFalse()
	{	
		// there's a session sent to watson => return false
		$_object = new \stdClass();
		$_object->created = '2016-11-01 12:00:00';
		
		$mockCoreModel = $this->getMock('Model\ChatSessionModel', array('getListsEntities', 'count'));
		$mockCoreModel->expects($this->any())->method('getListsEntities')->will($this->returnValue([$_object]));
		$mockCoreModel->expects($this->any())->method('count')->will($this->returnValue(1));
		$this->assertEquals($mockCoreModel->sendToIA(4), false);
		
		// 4 == 4 => false
		$mockCoreModel->expects($this->any())->method('count')->will($this->returnValue(4));
		$this->assertEquals($mockCoreModel->sendToIA(4), false);
		
		
	}
	
	public function testSendToIATrueAfterRatio()
	{	
		// there's a session sent to watson and the count is > ratio => return true
		$_object = new \stdClass();
		$_object->created = '2016-11-01 12:00:00';
		
		$mockCoreModel = $this->getMock('Model\ChatSessionModel', array('getListsEntities', 'count'));
		$mockCoreModel->expects($this->any())->method('getListsEntities')->will($this->returnValue([$_object]));
		// there are 5 objects => the next is true
		$mockCoreModel->expects($this->any())->method('count')->will($this->returnValue(5));
		$this->assertEquals($mockCoreModel->sendToIA(4), true);
		
		
	}
	
	public function testCreateChatSessionIa()
	{
		$mockCoreModel = $this->getMock('Model\ChatSessionModel', array('createToken', 'sendToIA', 'watsonRate', 'saveEntity'));
		$mockCoreModel->expects($this->exactly(1))->method('createToken')->will($this->returnValue('ABCDE'));
		$mockCoreModel->expects($this->exactly(1))->method('sendToIA')->will($this->returnValue(true));
		$mockCoreModel->expects($this->exactly(1))->method('watsonRate')->will($this->returnValue(4));
		$mockCoreModel->expects($this->exactly(1))->method('saveEntity')->will($this->returnValue(null));
		$sess = $mockCoreModel->createChatSession();
		$this->assertNull($sess->cc_start);
		$this->assertNull($sess->ia_start);
		
	}
	
	public function testCreateChatSessionCC()
	{
		$mockCoreModel = $this->getMock('Model\ChatSessionModel', array('createToken', 'sendToIA', 'watsonRate', 'saveEntity'));
		$mockCoreModel->expects($this->exactly(1))->method('createToken')->will($this->returnValue('ABCDE'));
		$mockCoreModel->expects($this->exactly(1))->method('sendToIA')->will($this->returnValue(false));
		$mockCoreModel->expects($this->exactly(1))->method('watsonRate')->will($this->returnValue(4));
		$mockCoreModel->expects($this->exactly(1))->method('saveEntity')->will($this->returnValue(null));
		$sess = $mockCoreModel->createChatSession();
		$this->assertNull($sess->cc_start);
		$this->assertNull($sess->ia_start);
	}
	
	public function testCreateToken()
	{
		$chatsession = new ChatSessionModel();
		$this->assertNotNull($chatsession->createToken());
		$this->assertNotEmpty($chatsession->createToken());
		
	}

	public function testGetSessionData()
	{
		global $chatSessionModel;

		$entity = $chatSessionModel->createChatSession();
		$token = $entity->token;
		$entity2 = $chatSessionModel->getSessionData($token);
		$this->assertEquals($entity->token, $entity2->token);
	}

	public function testUpdateSessionData()
	{
		global $chatSessionModel;

		$entity = $chatSessionModel->createChatSession();
		$session_id = $entity->token;
		$session_creds = array('emergia_session_token' => '1234567','emergia_chat_token' =>'654321');
		$chatSessionModel->updateSessionData($session_id, $session_creds);
		$entity2 = $chatSessionModel->getSessionData($session_id);
		$this->assertEquals($entity2->emergia_session_token, '1234567');
		$this->assertEquals($entity2->emergia_chat_token, '654321');

	}

	public function testUpdateSessionDataBad()
	{
		global $chatSessionModel;

		$entity = $chatSessionModel->createChatSession();
		$session_id = 'invalid';
		$session_creds = array('emergia_session_token' => '1234567','emergia_chat_token' =>'654321');
		$chatSessionModel->updateSessionData($session_id, $session_creds);
		$entity2 = $chatSessionModel->getSessionData($session_id);
		$this->assertNull($entity2);
	}

	public function testUpdateChatHistory()
	{
		global $chatSessionModel;

		$entity = $chatSessionModel->createChatSession();
		$session_id = $entity->token;
		$chat_record = array('data' => date('Y-m-d H:i'), 'text' => 'This is a test. 98654321', 'origin' => 'user');
		$chat_record2 = array('data' => date('Y-m-d H:i'), 'text' => 'This is a test. 123456789', 'origin' => 'operator');
		$chatSessionModel->updateChatHistory($session_id, $chat_record);
		$chatSessionModel->updateChatHistory($session_id, $chat_record2);

		$entity2 = $chatSessionModel->getSessionData($session_id);

		$this->assertEquals(count($entity2->chat_history), 2);
		$this->assertEquals($entity2->chat_history[0]['text'], 'This is a test. 98654321');
		$this->assertEquals($entity2->chat_history[1]['text'], 'This is a test. 123456789');

	}

	public function testUpdateChatHistoryBad()
	{
		global $chatSessionModel;

		$chat_record2 = array('data' => date('Y-m-d H:i'), 'text' => 'This is a test. 123456789', 'origin' => 'operator');

		$entity = $chatSessionModel->updateChatHistory('fake', $chat_record2);

		$this->assertNull($entity);

	}
}

?>