<?php

namespace APIClient; 

/**
 * Description of EmergiaAPIClient
 *
 * @author krukmat
 */
class EmergiaAPIClient extends \APIClient\OAUTHClient
{	

	function __construct()
	{
		$this->uri = $this->getAppParam('application.presence.presence_api_url');
		$this->user = $this->getAppParam('application.presence.presence_user');
		$this->password = $this->getAppParam('application.presence.presence_password');
		$this->oauth_end = $this->getAppParam('application.presence.presence_oauth');
		$this->serviceId = $this->getAppParam('application.presence.presence_service');
	}

	public function checkResponse($response)
	{
		if ($response->Code == 0)
		{
			return array('Code' => $response->Code, 'Data' => $response->Data);
		}
		return array('Code' => $response->Code, 'ErrorMessage' => $response->ErrorMessage);
	}

	public function getChatSession($token, $name, $phone, $email, $priority=1, $language='ES', $remote_ip='', $customer_url='', $chat_data_redirected=0, $session_vars='', $previousSessionId=0, $sessionType='')
	{

		$data = array(
		  "ServiceId" => $this->serviceId,
		  "Name" => $name,
		  "Phone" => $phone,
		  "Email" => $email,
		  "Language" => $language,
		  "SessionType" => $sessionType,
		  "Priority" => $priority,
		  "RemoteIP" => $remote_ip,
		  "CustomerURL" => $customer_url,
		  "PreviousSessionId" => $previousSessionId,
		  "ChatDataRedirected" => $chat_data_redirected,
		  "SessionVars" => $session_vars
		);

		$response = \Httpful\Request::post($this->uri.'sessions')->sendsJson()->addHeader('Authorization', 'Bearer '.$token)->body(json_encode($data))->send();

		//var_dump($response);

		// session url
		if (($response->code == 200) || ($response->code == 201))
		{	$headers = $response->headers->toArray();
			if (isset($headers['location']))
			{
				$session_url = $headers['location'];
				return $session_url;
			}
		}
		
		return null;
	}

	private function mockOnlineSessionStatus()
	{

		// TODO: Borrar datos fake
		$_object = new \stdClass();
		$_object->State = 1;
		$_object->AgentNickName = 'Matias';
		$_object->IsTyping = true;
		$_object->Text = ['Hola soy Matias, ¿que necesita?'];
		$_object->AgentId = 1000;

		$_response = new \stdClass();
		$_response->Code = 0;
		$_response->Data = $_object;

		return $_response;
	}

	private function mockOfflineSessionStatus()
	{
		// TODO: Borrar datos fake
		$_object = new \stdClass();
		$_object->State = 3;
		$_object->AgentNickName = 'Matias';
		$_object->IsTyping = true;
		$_object->Text = ['Hola soy Matias, ¿que necesita?'];
		$_object->AgentId = 1000;

		$_response = new \stdClass();
		$_response->Code = 0;
		$_response->Data = $_object;

		return $_response;
	}

	public function getSessionStatus($token, $session_url)
	{
		$response = json_decode (\Httpful\Request::get($session_url.'/status')->addHeader('Authorization', 'Bearer '.$token)->send());

		// TODO: Fake data quitar!
		$fake_response = $this->mockOnlineSessionStatus();
		return $this->checkResponse($fake_response);

		if (isset($response))
			return $this->checkResponse($response);
		return $response;
	}

	public function getChatInfo($token, $session_url)
	{
		$response = json_decode (\Httpful\Request::get($session_url.'/')->addHeader('Authorization', 'Bearer '.$token)->send());
		if (isset($response))
			return $this->checkResponse($response);
		return $response;
	}

	public function postMessage($token, $session_url, $text)
	{
		$response = \Httpful\Request::post($session_url.'/text')->sendsJson()->addHeader('Authorization', 'Bearer '.$token)->body(json_encode($text))->send();
		return $response->code == 204 or $response->code == 200;
	}

	public function putUserWriting($token, $session_url)
	{
		$response = \Httpful\Request::put($session_url.'/userwriting')->addHeader('Authorization', 'Bearer '.$token)->send();
		return $response->code == 204 or $response->code == 200;
	}

	public function deleteUserWriting($token, $session_url)
	{
		$response = \Httpful\Request::delete($session_url.'/userwriting')->addHeader('Authorization', 'Bearer '.$token)->send();
		return $response->code == 204 or $response->code == 200;
	}

	public function deleteChatSession($token, $session_url, $closeType)
	{	
		// TODO: Puede tirar Fatal error: Maximum execution time of 30 seconds exceeded. No sirve el catch
		set_time_limit(0); // Prevengo asi por ahora este error
		try
		{
			$closeType = array('CloseType' => $closeType);
			$response = \Httpful\Request::delete($session_url)->sendsJson()->addHeader('Authorization', 'Bearer '.$token)->body(json_encode($closeType))->send();
			return $response->code == 204 or $response->code == 200;
		}
		catch (\Exception $e) 
		{
			return true;
		}

	}

	public function deleteToken($token)
	{
		try 
		{
			$response = \Httpful\Request::delete($this->uri.'token')->addHeader('Authorization', 'Bearer '.$token)->send();
			return $response->code == 204 or $response->code == 200;
		}
		catch (\Exception $e) 
		{
			return true;
		}
	}

}