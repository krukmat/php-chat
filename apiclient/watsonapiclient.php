<?php

namespace APIClient; 

/**
 * Description of WatsonAPIClient
 *
 * @author krukmat
 */
class WatsonError extends \Exception
{
}

class WatsonAPIClient extends \APIClient\OAUTHClient
{	
	protected $error_codes = array(
								array('code' => 401, 'description' => 'Invalid Token'),
								array('code' => 403, 'description' => 'Access Denied'),
								array('code' => 500, 'description' => 'Error interno')
								);
	protected static $auth_const = 'Authorization';
	function __construct()
	{
		$this->uri = $this->getAppParam('application.watson.watson_api_url');
		$this->user = $this->getAppParam('application.watson.watson_user');
		$this->password = $this->getAppParam('application.watson.watson_password');
		$this->oauth_end = $this->getAppParam('application.watson.watson_oauth');
	}

	protected function getErrorMessage($response)
	{
		foreach ( $this->error_codes as $error_code)
		{
			if ($error_code['code'] == $response->code)
				return $response->code.':'.$error_code['description'];
		}
		return $response->code.':Unknown code';
	}

	protected function checkResponse($response)
	{	
		if ($response->code == 200)
		{
			return json_decode ($response);
		}
		throw new WatsonError($this->getErrorMessage($response));
	}

	public function init($token, $user_name) 
    {	
    	$message = array('input' => array('user_name' => $user_name));
		$response = \Httpful\Request::post($this->uri.'message')->sendsJson()->addHeader(WatsonAPIClient::$auth_const,'Bearer '.$token)->body(json_encode($message))->send();
		return $this->checkResponse($response);
	}

	public function chat($token, $like, $text, $conversation_id)
	{	
		$message = array('input' => array('text' => $text), 'like' => $like, 'conversation_id' => $conversation_id, 'timestamp' => date("Y-m-dTH:i:sZ"));
		$response = \Httpful\Request::post($this->uri.'message')->sendsJson()->addHeader(WatsonAPIClient::$auth_const, 'Bearer '.$token)->body(json_encode($message))->send();
		return $this->checkResponse($response);
	}
}