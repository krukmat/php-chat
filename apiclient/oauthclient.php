<?php
namespace APIClient; 

/**
 * Description of OAUTHClient
 *
 * @author krukmat
 */

class OAUTHClient
{	

	public function getToken($method = 'get') 
    {
		if ($method == 'get')
			$response = json_decode (\Httpful\Request::get($this->uri.$this->oauth_end)->authenticateWith($this->user, $this->password)->send());
		else {
			$creds = array('username' => $this->user, 'password' => $this->password);
			$response =json_decode (\Httpful\Request::post($this->oauth_end)->sendsJson()->body(json_encode($creds))->send());
			return $response;
		}

		if (isset($response))
			return $this->checkResponse($response);

		return null;
	}

	function getAppParam($parameter) 
	{
		global $application;
		return $application->config->getParam($parameter);
	}

}