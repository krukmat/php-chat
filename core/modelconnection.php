<?php

namespace Core;

/**
 * Description of connection
 *
 * @author aguilellaj
 */
class ModelConnection
{

    private $connection = null;
    private $database;

    /**
     * Sets the connection to noSQL server
     * @author aguilellaj
     * @since 2015-10-01
     * 
     * @param string $server
     * @param string $port
     * @param string $database
     */
    function __construct($server, $port, $database)
    {
        try {
            $this->connection = new \MongoClient($server . ":" . $port);
            if (!$this->connection || !$this->connection->connected) {
                // @codeCoverageIgnoreStart
                throw new \Exception("error connecting to NoSQL database");
                // @codeCoverageIgnoreEnd
            }

            $this->database = $this->connection->selectDB($database);
        } catch (Exception $ex) {
            // @codeCoverageIgnoreStart
            throw new \Exception("NoSQLConnection class doesnt init correct with message: " . $ex);
            // @codeCoverageIgnoreEnd
        }
    }

    /**
     * Return the list of ddbb from server
     * @author aguilellaj
     * @since 2015-10-01
     * 
     * @return array
     */
    function listDBs()
    {
        return $this->connection->listDBs();
    }
    
    function lastError(){
        return $this->database->lastError();
    }

    /**
     * Search in a collection
     * @author aguilellaj
     * @since 2015-10-01
     * 
     * @param string $collectionName
     * @param expression $filter
     * @param int $itemsPerPage
     * @param int $skip
     * @return array
     */
    function findInCollection($collectionName, $filter, $itemsPerPage = 1000000, $skip = 0, $orderBy = null)
    {
        $collection = $this->connection->selectCollection($this->database, $collectionName);
        
        if ($orderBy == null) {
            $cursor = iterator_to_array($collection->find($filter)->skip($skip)->limit($itemsPerPage));
        } else {
            $cursor = iterator_to_array($collection->find($filter)->sort($orderBy)->skip($skip)->limit($itemsPerPage));
        }
        
        $cant = $collection->find($filter)->count();
       
        return array($cursor, $cant);
    }

    /**
     * Insert one item in collection
     * @author aguilellaj
     * @since 2015-10-01
     * 
     * @param string $collectionName
     * @param class $entity
     */
    function insertInCollection($collectionName, $entity)
    {
        try {
            $collection = $this->connection->selectCollection($this->database, $collectionName);
            $collection->insert($entity);
        } catch (\Exception $ex) {
            
        }
    }

    /**
     * Update one item in collection
     * @author aguilellaj
     * @since 2015-10-01
     * 
     * @param string $collectionName
     * @param expression $filter
     * @param class $entity
     */
    function updateInCollection($collectionName, $filter, $entity)
    {
        try {
            $collection = $this->connection->selectCollection($this->database, $collectionName);
            $collection->update($filter, $entity);
        } catch (\Exception $ex) {
            
        }
    }

    /**
     * Return the count of items from collection
     * @author aguilellaj
     * @since 2015-10-01
     * 
     * @param string $collectionName
     * @return int 
     */
    function count($collectionName, $filter = null)
    {
        $collection = $this->connection->selectCollection($this->database, $collectionName);
        if($filter != null) {
            return $collection->find($filter)->count();
        }
        return $collection->count();
    }
    
    function min($collectionName, $field){
       $collection = $this->connection->selectCollection($this->database, $collectionName);
       $documents = iterator_to_array($collection->find()->sort(array($field => 1))->skip(0)->limit(1));
       
       if(count($documents) > 0) {
           return ((int)array_values($documents)[0][$field]);
       } 
       return 0;
    }
    
    function max($collectionName, $field){
       $collection = $this->connection->selectCollection($this->database, $collectionName);
       $documents = iterator_to_array($collection->find()->sort(array($field => -1))->skip(0)->limit(1));

       if(count($documents) > 0) {
           return ((int)array_values($documents)[0][$field]);
       } 
       return 0;
    }
    
    function delete($collectionName, $filter){
        $collection = $this->connection->selectCollection($this->database, $collectionName);
        $collection->remove($filter);
    }

}
