<?php

namespace Core;

/**
 * Description of Entities
 *
 * @author aguilellaj
 */
class Entity
{
    public $id = '';
    public $created;
    public $updated;
    
    function getKeyValue($idName)
    {
        return $this->$idName;
    }

    /**
     * Return the attributes of the entity without control attributes
     * @author aguilellaj
     * @since 2015-09-21 
     *
     * @return array
     */
    function getEntityAttributes($class)
    {
        $extendedAttributes = get_class_vars($class);
        $baseAttributes = get_class_vars('\Core\Entity');
        
        $entityAttributes = array_merge($extendedAttributes, $baseAttributes);
        
        return $entityAttributes;
    }

    /**
     * Convert the entity attributes to array
     * @author aguilellaj
     * @since 2015-09-25
     * 
     * @return array
     */
    function toArray()
    {
        $attributes = get_class_vars(get_class($this));

        $data = array();
        foreach ($attributes as $index => $attribute) {
            $data[$index] = $this->$index;
        }

        return $data;
    }

    /**
     * Return the  attributes of the class
     * @author aguilellaj
     * @since 2015-09-25
     * 
     * @return array
     */
    function getAttributes()
    {
        return array_keys($this->getEntityAttributes(get_class($this)));
    }

}
