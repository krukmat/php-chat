<?php

namespace Core;

/**
 * Include all basic functionalities to run the application
 *
 * @author aguilellaj
 */
class Application
{
    public $config;
    public $modelConnection;
    
    /**
     * Constructor of the class.
     */
    function __construct($config=null)
    {
        spl_autoload_register(__NAMESPACE__ . "\Application::autoload");
        $this->setEnvironmentVars();
        
        //Init the config class to set all parameters
        if ($config)
            $this->config = $config;
        else
            $this->config = new \Core\Config($this->getDocumentRoot().'config'.DIRECTORY_SEPARATOR.'config.ini');
        
        $this->modelConnection = new \Core\ModelConnection($this->config->getParam('connection.mongodb.server'), 
                                        $this->config->getParam('connection.mongodb.port'), 
                                        $this->config->getParam("connection.mongodb.database"));
    }

    /**
     * Get the root folder of the app
     * @author aguilellaj
     * 
     * @return string
     */
    public static function getDocumentRoot()
    {
        return dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR;
    }
    
    /**
     * Load the class using the namespace and the class name
     * @author aguilellaj
     * 
     * @param string $class name of the class to be loaded 
     */
    private static function autoload($class)
    {
        $classpath = strtolower($class) . ".php";

        $classpath = str_replace("\\", DIRECTORY_SEPARATOR, $classpath);

        if (file_exists(Application::getDocumentRoot() . $classpath)) {
            require_once(Application::getDocumentRoot() . "$classpath");
        }
    }

    /**
     * Define the environment variables based on the domain
     * @author aguilellaj
     */
    public function setEnvironmentVars()
    {
        defined('APPLICATION_ENV')
        || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
    }


    /**
     * Gets a param from request
     * @author aguilellaj
     * 
     * @param type $id
     * @return string
     */
    //@codeCoverageIgnoreStart
    public function getRequestParam($id)
    {
        $value = filter_input(INPUT_GET, $id, FILTER_UNSAFE_RAW);
        if ($value == '') {
            $value = filter_input(INPUT_POST, $id, FILTER_UNSAFE_RAW);
        }
        return $value;
    }
    //@codeCoverageIgnoreEnd
}
