<?php
namespace Core;

require_once 'entity.php';


/**
 * Description of Entity Tests
 *
 * @author krukmat
 */

 class CoreTest extends \PHPUnit_Framework_TestCase
{
	public function testToArray()
	{
		$entity = new Entity();
		$entity->id = 1234;
		$entity->created = '1234';
		$entity->updated = '6543';

		$array = $entity->toArray();

		$this->assertEquals($array['id'], 1234);
		$this->assertEquals($array['created'], '1234');
	}
}