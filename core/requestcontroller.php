<?php

namespace Core;

/**
 * Include all basic functionalities to run the application
 *
 * @author aguilellaj
 */
class RequestController
{
    var $controller;
    var $method;
    var $language;
    var $accept;
    var $params;
    
    /**
     * Inits the language, the accept response, and controller and method.
     */
    function __construct()
    {
        $this->language = filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE');
        $this->accept = filter_input(INPUT_SERVER, 'HTTP_ACCEPT');
        
        $this->parseUrl();
        $this->setMethod(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
    }
    
    /**
     * Execute the action
     */
    function execute()
    {
        //call_user_func_array(array(new $this->controller, $this->method), $this->params);
        call_user_func_array(array(new $this->controller, $this->method), $this->params);
    }
    
    /**
     * Parse the url and get the resource and the params
     * @author aguilellaj
     */
    function parseURL()
    {
        $path = trim(parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'), PHP_URL_PATH), "/");
        
        $this->controller = '';
        $this->params = array();
       
        if ($path != '') {
            $urlParts = explode("/", $path);
            
            if (count($urlParts) == 1) {
                list($controller) = $urlParts;
                $params = array();
            }
            
            if (count($urlParts) == 2) {
                list($controller, $params) = $urlParts;
            }
            
            $this->setController($controller, $params);
        } else {
            $baseController = new BaseController();
            $baseController->error404();
        }
    }
    
    /**
     * Set the controller after validate if exists
     * @param string $controller
     */
    function setController($controller, $params)
    {
        $controllerName = $this->getControllerPath($controller);
        if (!class_exists($controllerName)) {
            $baseController = new BaseController();
            $baseController->error404();
        }
        $this->controller = $controllerName;
        
        if ($params != '') {
            if (count($params) > 0) {
                $this->setParams(explode("/", $params));
            }
        }
    }
    
    /**
     * Set the params to the used
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }
    
    /**
     * Set the method after validate if exists
     * @author aguilellaj
     * @param string $method
     */
    function setMethod($method){
        $reflector = new \ReflectionClass($this->controller);
        if (!$reflector->hasMethod($method)) {
            $baseController = new BaseController();
            $baseController->error405();
        }
        $this->method = $method;
    }
    
     /**
     * Generated the complete controller name based on resource name
     * @author aguilellaj
     * @param string $controllerName
     * @return string
     */
    public static function getControllerPath($controllerName)
    {
        return "\\Controller\\" . ucfirst(strtolower($controllerName)) . "Controller";
    }
    
    
}
