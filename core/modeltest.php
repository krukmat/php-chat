<?php
namespace Model;


require_once("core".DIRECTORY_SEPARATOR."application.php");

require_once 'model.php';
require_once 'config.php';
require_once 'application.php';
require_once 'entity.php';


/**
 * Description of ModelTest Tests
 *
 * @author krukmat
 */

abstract class ModelTest extends \PHPUnit_Framework_TestCase
{

	protected function setUp()
    {
        global $application, $config;
        $config = new \Core\Config('config/config_test.ini');
		$application = new \Core\Application($config);
    }

    public function testLoadEntityWithId()
    {
        $entity = new \Core\Entity();
        $entity->id = 1;
        $model = new \Core\Model();
        $entity2 = $model->loadEntity($entity, 1);
    }

    public function testInvalidConfigSetting()
    {
        global $config;
        $this->setExpectedException(\Exception::class);
        $config->getParam('lalala');
    }

    public function testInvalidSettingFile()
    {   
        $this->setExpectedException(\Exception::class);
        $config = new \Core\Config('config/fake.ini');
    }

    public function testGetParam()
    {   global $config;
        $local_config = array('development' => array('application.version' => '0.1'));
        $config->setConfigParams($local_config);
        $config->getParam('application.version');
    }

    public function testEmptyConfigInApplication()
    {
        $application = new \Core\Application();
        $this->assertNotNull($application->config);
    }

    protected function tearDown()
    {
        global $application, $modelName;
        $application->modelConnection->delete($modelName, []);
    }
}
