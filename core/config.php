<?php

namespace Core;
/**
 * Reads the config file and set the params based on environment
 *
 * @author aguilellaj
 */
class Config
{

    const defaultSection = 'default';

    private $configParams;

    /**
     * Constructor of the class, load the config file
     * @author aguilellaj
     * 
     * @param string $file config file path
     */
    function __construct($file)
    {
        if(!file_exists($file)){
            throw new \Exception("the config file couldn't be loaded");
        }
        $this->configParams = parse_ini_file($file, true);
    }

    public function setConfigParams($config)
    {
        $this->configParams = $config;
    }

    /**
     * Get a param from config
     * @author aguilellaj
     * 
     * @param string $key key of the value
     */
    public function getParam($key)
    {
        if (isset($this->configParams[APPLICATION_ENV][$key])) {
            return $this->configParams[APPLICATION_ENV][$key];
        } elseif (isset($this->configParams[Config::defaultSection][$key])) {
            return $this->configParams[Config::defaultSection][$key];
        } else {
            throw new \Exception('The especific param ' . $key . ' doesn\'t exists in config file. Please add this at least at default section');
        }
    }
    
}
