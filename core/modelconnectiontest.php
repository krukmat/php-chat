<?php
namespace Core;

require_once("core".DIRECTORY_SEPARATOR."application.php");

require_once 'model.php';
require_once 'config.php';
require_once 'application.php';
require_once 'entity.php';


/**
 * Description of ModelConnection Tests
 *
 * @author krukmat
 */

 class ModelConnectionTest extends \PHPUnit_Framework_TestCase
{
	protected function setUp()
    {
        global $application;
        $config = new \Core\Config('config/config_test.ini');
		$application = new \Core\Application($config);
    }

    public function testListDbs()
    {
    	global $application;

    	$dbs = $application->modelConnection->listDBs();

    	$this->assertGreaterThan(0, count($dbs));
    }

    public function testInvalidConnection()
    {
    	global $application;

    	$this->setExpectedException(\Exception::class);
    	$application->modelConnection = new ModelConnection('localhost', '1234', 'fffff');
    }

    public function testLastError()
    {
    	global $application;
    	$lastError = $application->modelConnection->lastError();
    	$this->assertGreaterThan(0, count($lastError));
    }

    public function testMinMax()
    {   
        global $application;

        // Empty values
        $this->assertEquals($application->modelConnection->min('integer_list', 'value'), 0);

        $this->assertEquals($application->modelConnection->max('integer_list', 'value'), 0);

        $integer_list = array(1,2,3,4,5,6,7,8,9);
        foreach($integer_list as $value )
        {
            $_object = new \stdClass();
            $_object->value = $value;
            $application->modelConnection->insertInCollection('integer_list', $_object);
        }

        // Minimos y Maximos
        $this->assertEquals($application->modelConnection->min('integer_list', 'value'), 1);

        $this->assertEquals($application->modelConnection->max('integer_list', 'value'), 9);
    }

    protected function tearDown()
    {
        global $application;
        $application->modelConnection->delete('integer_list', []);
    }
}