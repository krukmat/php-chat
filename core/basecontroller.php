<?php

namespace Core;

/**
 * Description of BaseController
 *
 * @author aguilellaj
 */
class BaseController
{
    /**
     * Gets a param from request
     * @author aguilellaj
     * 
     * @param type $id
     * @return string
     */
    public function getRequestParam($id, $default = "")
    {
        $value = filter_input(INPUT_GET, $id, FILTER_UNSAFE_RAW);
        if ($value == '') {
            $value = filter_input(INPUT_POST, $id, FILTER_UNSAFE_RAW);
        }
        
        if($value == "" && $default != ''){
            $value = $default;
        }
        return $value;
    }
    
    /**
     * Gets a array param from request
     * @author aguilellaj
     * 
     * @param type $id
     * @return array    
     */
    public function getRequestArray($id)
    {
        $value = filter_input(INPUT_GET, $id, FILTER_DEFAULT,  FILTER_FORCE_ARRAY);
        if ($value == '') {
            $value = filter_input(INPUT_POST, $id, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        }
        return $value;
    }
    
    /**
     * Gets the json in body request and return an associative array
     * @author aguilellaj
     * @return array
     */
    public function getJSONBody()
    {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, true);
        
        return $input;
    }
    
    /**
     * Return an success 201 (entity created correctly) and finish the process
     * @author aguilellaj
     */
    public function success200()
    {
        http_response_code(200);
        die();
    }
    
    /**
     * Return an success 201 (entity created correctly) and finish the process
     * @author aguilellaj
     */
    public function success201()
    {
        http_response_code(201);
        die();
    }
    
    /**
     * Return an success 205 (response empty) and finish the process
     * @author aguilellaj
     */
    public function success205()
    {
        http_response_code(205);
        die();
    }
    
    /**
     * Return an error 400 (bad request) and finish the process
     * @author aguilellaj
     */
    public function error400()
    {
        http_response_code(400);
        die();
    }
    
    /**
     * Return an error 404 (not found) and finish the process
     * @author aguilellaj
     */
    public function error404()
    {
        http_response_code(404);
        die();
    }
    
    /**
     * Return an error 405 (method not allowed) and finish the process
     * @author aguilellaj
     */
    public function error405()
    {
        http_response_code(405);
        die();
    }
    
    function defaultPOSTValue($var, $default){
        if (isset($_POST[$var]) && strlen($_POST[$var])>0) {
            return $_POST[$var];
        }
        return $default;
    }

    function defaultGETValue($var, $default){
        if (isset($_GET[$var]) && strlen($_GET[$var])>0) {
            return $_GET[$var];
        }
        return $default;
    }

    public function setDriver($which='emergia')
    {
        if ($which == 'emergia')
        {
            $this->driver = new \Drivers\EmergiaDriver();
        }

        if ($which == 'watson')
        {
            $this->driver = new \Drivers\WatsonDriver();
        }
    }

    public function validateSession($with_operation=true)
    {   
        
        $session_id = $_GET['session_id'];

        if ($with_operation)
        {
            $this->operation = $_GET['operation'];
            // Si no existe operacion o session_id => 404
            if (!isset($this->operation) || (!isset($session_id)))
                $this->error404();
        } else {
            if (!isset($session_id))
                $this->error404();
        }

        $session_data = $this->chat_session->getSessionData($session_id);

        // Existe la sesion? sino => 404
        if  (!isset($session_data))
            $this->error404();

        // Si aun no se definio el ia_end y send_to_ia esta en true => el driver es watson.
        if (($session_data->send_to_ia == true) && !(isset($session_data->ia_end))) {
            $this->setDriver('watson');
        }
        else{
            $this->setDriver();
        }

        return $session_data;
    }
    
}
