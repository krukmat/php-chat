<?php

namespace Core;

/**
 * Description of Model
 *
 * @author aguilellaj
 */
class Model
{
    public $idName = 'id';
    public $modelName = 'base';

    /**
     * Return the name of the model that saves the entity
     * 
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Save the entity in db, if has id, make update else an insert
     * 
     * @param class $entity
     * @global \Core\Application $application
     */
    function saveEntity($entity)
    {
        global $application;

        $filter = array($this->idName => $entity->getKeyValue($this->idName));
        $cant = $application->modelConnection->count($this->modelName, $filter);
        if ($cant == 0) {
            $entity->created = date("Y-m-d H:i:s");
            $application->modelConnection->insertInCollection($this->modelName, $entity);
        } else {
            $entity->updated = date("Y-m-d H:i:s");
            $application->modelConnection->updateInCollection($this->modelName, $filter, $entity);
        }
    }

    /**
     * Save the entity in db, if has id, make update else an insert
     * 
     * @param class $entity
     */
    function LoadEntity($entity, $id, $key = '')
    {
        if($key == '') {
            $filter = array("id" => $id);
        } else {
            $filter = array($key => $id);
        }
        
        $products = $this->getListsEntities($entity, $filter);
        
        if ($products != null && count($products) > 0) {
            foreach($products[0]->getAttributes() as $attribute){
                $entity->$attribute = $products[0]->$attribute;
            }
        }
    }
    
   /**
     * Return a list of entities
     * 
     * @global \Core\Application $application
     * @param string $entityName
     * @param string $sql
     * @param bool $indexify
     * @return array
     */
    function getListsEntities($entity, $filter, $numItems = 1000000, $page = 0, $orderBy = null){
        global $application; 

        $result = array();
        
        list($documents, $cant) = $application->modelConnection->findInCollection($this->modelName, $filter, $numItems, $page, $orderBy);

        if(is_string($entity)) {
            $classEntity = "\\Entities\\".$entity;
        } else {
            $classEntity = get_class($entity);
        }
        
        if ($cant > 0) {
            foreach($documents as $document){
                $entity = new $classEntity();
                $attributes = $entity->getAttributes();
                
                foreach ($document as $key => $value) {
                    if (in_array($key, $attributes)) {
                        if (is_a($value, 'MongoDate'))
                            $entity->$key = date('Y-m-d', $value->sec);
                        else
                            $entity->$key = $value;
                    }
                }
                $result[] = $entity;
            }
            
            return $result;
        }
        
        return false;
    }
    
    public function count($filter)
    {
        global $application;
        
        return $application->modelConnection->count($this->modelName, $filter);
    }

    public function delete($filter)
    {
        global $application;
        
        return $application->modelConnection->delete($this->modelName, $filter);
    }

    public function search($entity, $filter, $only_one = true)
    {
        $entities = $this->getListsEntities($entity, $filter);
        
        if (($entities) && (count($entities) > 0)) {
            if ($only_one)
                return $entities[0];
            return $entities;
        }

        return null;
    }

    public function createToken()
    {   // @codeCoverageIgnoreStart
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        // @codeCoverageIgnoreEnd

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}