<?php

namespace Core;

/**
 * Description of viewRender
 *
 * @author aguilellaj
 */
class RenderView
{

    private $data = array();
    private $render = FALSE;
    private $template = 'default';
    private $templateRoot;
    public $breadcrumb;
    public $scripts;
    public $css;
    public $renderOnDestroy = true;
    public $menu;
    public $currentUser;

    /**
     * Initialize the view params, the template
     * @author aguilellaj
     * 
     * @param type $template
     * @throws \Exception
     */
    public function __construct($template, $templateBase = '')
    {
        try {
            $this->templateRoot = Application::getDocumentRoot() . 'view/';
            $file = $this->templateRoot . strtolower($template) . '.php';
            
            $file = str_replace("\\", DIRECTORY_SEPARATOR, $file);

            if ($templateBase != '') {
                $this->template = $templateBase;
            }
            
            if (file_exists($file)) {
                $this->render = $file;
            } else {
                echo $file;
                throw new \Exception('Template ' . $template . ' not found!');
            }
        } catch (customException $e) {
            echo $e->errorMessage();
        }
    }

    /**
     * Set the breadcrumb
     * @author aguilellaj
     * @since 2015-09-22
     * 
     * @param array $breadcrumb
     * @return \Core\FrontController
     */
    public function setBreadcrumb(array $breadcrumb)
    {
        //Alwais add the link to home
        $this->assign('breadcrumb', $breadcrumb);
    }

    /**
     * Set the scripts 
     * @author aguilellaj
     * @since 2015-10-05
     * 
     * @param array $scripts
     */
    public function setScripts(array $scripts)
    {
        $this->assign('scripts', $scripts);
    }

    /**
     * Set the CSS
     * @author aguilellaj
     * @since 2015-10-08
     * @param array $css
     */
    public function setCSS(array $css)
    {
        $this->assign("css", $css);
    }

    /**
     * Assign a variable to be rended in view
     * @author aguilellaj
     * @since 2015-09-22
     * 
     * @param type $variable
     * @param type $value
     */
    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    /**
     * Render the view and force the download
     * @author aguilellaj
     * @since 2015-10-22
     * 
     * @param string $filename
     */
    public function download($filename)
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Content-Type: text/xml");

        include($this->templateRoot . 'templates/' . $this->template . ".php");

        $this->renderOnDestroy = false;
    }
    
    /**
     * Render the view and save in specific filder
     * @author aguilellaj
     * @since 2015-12-28
     * 
     * @param string $filename
     */
    public function save($filename)
    {
        $this->renderOnDestroy = false;
        
        ob_start();
        
        include($this->templateRoot."templates/" . $this->template . ".php");
        
        $content = ob_get_clean();
        file_put_contents( $filename, $content);
    }

    /**
     * At the end of the object, render the template with the view
     */
    public function __destruct()
    {
        if ($this->renderOnDestroy) {
            include($this->templateRoot . 'templates/' . $this->template . ".php");
        }
    }

}
