var conf =
{
    domain: 'localhost',
    server: 'localhost',
    protocol: 'http',
    port: '80',
    debug: false,
    lang_default: 'en', //Default selected lang in 'options', for change current language go to script tag in 'index.html'.
    lang:
    [
        {
            text: 'Spanish',
            i18n: 'es'
        },
        {
            text: 'English',
            i18n: 'en'
        },
        {
            text: 'French',
            i18n: 'fr'
        }
    ]
}
