function chat_clean() {
  $('.direct-chat-messages').html('');
}

function new_operator_message(agent_id, agent_name, is_typing, text, show_window) {
  // levantar ventana de chat
  // mostrar isTyping en el cuadro del chat
  // mostrar el nuevo texto en el cuadro de chat
  if (show_window)
    show_chat(agent_id, agent_name, session_id, false);
  var chat_window   = current_chat();
  // Solo si hay texto
  if (text)
    append_msg_he( text, chat_window, agent_name, standard_date(new Date()) );
  set_position();
}

function agent_new_chat(response) {
    if (response) {
      var chat_window   = current_chat();
      append_msg_he( response, chat_window, agent_name, standard_date(new Date()) );
    }
}

function do_is_typing(is_typing) {
  var chat_window = current_chat();
  if (is_typing)
    chat_window.parent().find("#iswriting").first().removeClass('no-display');
  else
    chat_window.parent().find("#iswriting").first().addClass('no-display');
}

function show_chat_dialog(iduser, name) {
  remove_bounce();
  open_chat_box ( iduser, name );

  //Set position
  set_widget_position_over_icon(current_chat(),  "flip, none");
  $( "#main-users-resizer" ).hide();
}

function disable_chat_input() {
  var textArea = current_chat().find( "#textarea_msg" ).first();
  textArea.attr("disabled", "disabled");
}

function create_chat_dialog ( user ) {
    if ( current_chat().length == 0 ) {
      $( "body" ).append( "\
        <div id='Dialog" + session_id + "' title='' user='" + user + "' class='no-padding'>\
        </div>");
      $("#chat").appendTo("#Dialog" + session_id);
      $("#chat").removeClass("no-display");
    }
}

function toggle_chat_dialog(toggle) {
  main_window_toggle();
  if (toggle) {
    current_chat().parent().toggle();
    return;
  }
  if (current_chat()) {
    current_chat().parent().hide();
  }
}

//Box Dialog for new user
  function configure_chat_dialog ( id, user ) {
    
    var dialog_text = i18n.title;

    current_chat().dialog({
      autoOpen: false,
      closeOnEscape: false,
      resizable: false,
      modal: false,
      draggable: false,
      minHeight: 200,
      maxHeight: 400,
      height: "auto",
      width: 380,

      open: function(event, ui) {
        
        //Set language in Dialog and init
        configure_chat_header_data( $( this ) );


          //Save the top of dialog
          main = $( this );
          var name = main.data( "name" );
          
          main.parent().find( ".ui-dialog-titlebar-close" ).addClass("ui-widget-header" );
          main.parent().find( ".ui-dialog-titlebar-close" ).addClass("no-border");
          main.parent().find( ".ui-dialog-titlebar" ).append($('#header-buttons').html());
          if (main.parent().find('#chat-header-title-ui').length === 0)
            main.parent().find( "#chat-header" ).append( "<div id='chat-header-title-ui' class='ui-widget'><div id='chat-header-title-id' class='chat-main-title'>" + dialog_text + "</div></div>" );
              
          //Set hide 'is writing...'
          main.parent().find( "#iswriting" ).first().addClass( "no-display" );

          //Change flag at init option
          //main.data( "init", 1 );

          //Textarea click
          main.parent().find( "textarea" ).click(function() {
            reset_unread_messages();
            remove_bounce();
          });

          //Minimize button
          main.parent().find( ".minimize-window" ).click(function() {
            $( "#main-users-resizer" ).toggle();
            is_minimized = $( "#main-users-resizer" ).is(':visible');
          });

          //Fancy scrollbar
          new_scroll(main.parent().find( ".direct-chat-messages" ));

          var typingTimeoutIsDoneWriting, typingTimeoutIsWriting;
          var textArea = main.find( "textarea" ).first();

          // TODO: mandar is writing
          textArea.keydown(function( e ) {
            if (typingTimeoutIsWriting !== undefined) {
              clearTimeout(typingTimeoutIsWriting);
            }
            typingTimeoutIsWriting = setTimeout(function() { do_is_writing(session_id, 
              function(){
                // error
                update_tips( i18n.connection_error );
                textArea.attr("disabled","disabled"); 
            }); console.log('writing'); }, 200);
          });

          // TODO: mandar done writing.
          // TODO: Mandar a logic los timers
          textArea.keyup(function( e ) {

            if (typingTimeoutIsDoneWriting !== undefined) {
              clearTimeout(typingTimeoutIsDoneWriting);
            }
            typingTimeoutIsDoneWriting = setTimeout(function() { do_is_done_writing(session_id, 
              function(){
                // error
                update_tips( i18n.connection_error );
                textArea.attr("disabled","disabled"); 
            }); console.log('end writing'); }, 700);

            //Intro event
            if ( (e.which == 13) && !event.shiftKey ) {
              var msg = clean_msg ( $( this ).val() );
              $( this ).val( "" );
              if (msg !== "") {
                // Mandar texto al backoffice
                append_msg_me(msg, main, standard_date(new Date()));
                set_widget_position_over_icon(main,  "none, none");
                do_post_message(msg);
              }
            }
            return false;
          });

        //}
        //Go to bottom
        new_scroll($( this ).parent().find( ".direct-chat-messages" ));
      },
    
      show: {
        effect: "none"
      },
      hide: {
        effect: "none"
      }
    });
}

function current_chat() {
  console.log(session_id);
	return $( "#Dialog" + session_id );
}

//Change text for lang
function configure_chat_header_data ( dialogid ) {
	dialogid.parent().find( "#iswriting-text" ).first().text( clean_name( dialogid.data( "name" ) ) + " " + i18n.is_writing );//Set text 'is writing...'
	dialogid.parent().find( "#agent_name" ).first().text( "Tu asesor es " +  clean_name( dialogid.data( "name" ) ) );//Set text 'is writing...'
	dialogid.parent().find( ".minimize-window" ).attr( "title", i18n.minimize );
	dialogid.dialog( { closeText: i18n.close } );
}

// Append my messages
function append_msg_me ( msg, main, date ) {
	var box = main.parent().find(".box-body");
	var me = box.find('.direct-chat-messages').last();
	var scroll = main.parent().find( ".direct-chat-messages" );


	if (me.children().last().attr('id') == 'me') {
	  me.children().find('.direct-chat-text').last().find('.direct-chat-timestamp').remove();
	  me.children().find('.direct-chat-text').last().append("<div class='chat-msg-me'>" + msg + "</div><span class='direct-chat-timestamp pull-right white'>" + convert_date(date.toString()) + "</span>")
	  //me.children().find('.direct-chat-text').last().append("<div>" + msg + "</div>")
	} else {
	  me.append("\
	    <div class='direct-chat-msg right' id='me'>\
	      <div class='direct-chat-info clearfix'>\
	      </div>\
	      <div class='direct-chat-text'>\
	        <div class='chat-msg-me'>" + msg + "</div>\
	        <span class='direct-chat-timestamp pull-right white'>" + convert_date(date.toString()) + "</span>\
	      </div>\
	    </div>");
	};

	// Go to bottom
	new_scroll(scroll);
}
  
function append_msg_he ( msg, main, name, date ) {
	var box = main.parent().find(".box-body");
	var he = box.find('.direct-chat-messages').last();
	var scroll = main.parent().find( ".direct-chat-messages" );

	if (he.children().last().attr('id') == 'he') {
	  he.children().find('.direct-chat-text').last().find('.direct-chat-timestamp').remove();
	  he.children().find('.direct-chat-text').last().append("<div class='chat-msg'>" + msg + "</div><span class='direct-chat-timestamp pull-right'>" + convert_date(date.toString()) + "</span>");
	} else {
	  he.append("\
	    <div class='direct-chat-msg' id='he'>\
	      <div class='direct-chat-info clearfix'>\
	      </div>\
	      <div class='direct-chat-text'>\
	        <img class='direct-chat-img' src='images/spinner-mediamrkt.png' alt='message user image' />\
	        <div class='chat-msg'>" + msg + "</div>\
	        <span class='direct-chat-timestamp pull-right'>" + convert_date(date.toString()) + "</span>\
	      </div>\
	    </div>");
	};
	// Go to bottom
	new_scroll(scroll);
}

  function change_chat_title(title) {
    $( "#chat-main-title-id" ).text( title );
  }

 function change_chat_operator_name(name) {
  current_chat().data( "name", name );
  configure_chat_header_data( current_chat() );
}

//Function for Open chat box
function open_chat_box ( iduser, name ) {
	if ( current_chat().dialog( "isOpen" ) == false ) {
	  //Close all dialogs
	  $( ".ui-dialog-content" ).dialog( "close" );
	  current_chat().data( "name", name );
	  current_chat().dialog( "open" );
	  $( "#main-users-resizer" ).hide();
	  // Set position on resize of the window
	  $( window ).resize(function() {
	    set_widget_position_over_icon(current_chat());
	  });
	} else
	  current_chat().dialog( "close" );
	  $( window ).resize(function() {
	  });  
}

function closing_confirm_dialog() {
  var confirm = $(document).find('#closing-dialog').clone();
          confirm.find('#confirm-button').click(function(){
          confirm.dialog("close");
          $('#chat_status').show();
          $( "#main-users-resizer" ).hide();
          //desconectar chat
          show_survey_form(session_id);
      })
  confirm.find('#cancel-button').click(function(){
      confirm.dialog("close");
      current_chat().parent().css('z-index', 50000);
      $('#chat_status').show()
  })
  
  current_chat().parent().css('z-index', 100);
  
  confirm.find('#textoExplicacion-closing-dialog').html(i18n.closing_dialog_confirm);
  confirm.removeClass('no-display');
  $('#chat_status').hide();
  
  confirm.dialog({
    modal: true,
    resizable: false,
    title: 'Atencion'
  });

}

  function show_chat(operator_id, operator_nickname, session_id, ping){
      create_chat_dialog( operator_id, operator_nickname ); 
      configure_chat_dialog( operator_id, operator_nickname );
      show_chat_dialog(operator_id, operator_nickname);
      change_chat_operator_name(operator_nickname);

      if (complete_chat_history){
        restore_chat_history(complete_chat_history);
        // limpiar el historial
        complete_chat_history = [];
      }

      // poner online el icono
      set_online();

      hide_widgets();

      console.log('hide_widgets');

      // Esto hace que no se vea la ventana de chat al restaurar sesion al recargar la pagina
      if (restored_session) {
        current_chat().parent().toggle();
        restored_session = false;
      }
      if (ping)
        assign_notificator(function(){chat_ping(session_id)}, 5000);
    }

