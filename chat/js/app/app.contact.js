function show_contact_form() {
   hide('#login');
   hide('#users');
   hide_widgets();
   add_contact_form_text(contact_form_text);
   change_chat_title(i18n.contact_form_title);
   show('#contact');
   create_contact_form( function(contact_name, contact_email, contact_comments) {
      post_contact(contact_name, contact_email, contact_comments, function() {
          // Mostrar el thanks
          hide_widgets();
          $.when(change_chat_title(i18n.contact_form_thanks_title), add_contact_thanks_form_text(i18n.contact_form_thanks_text), show('#contact_thanks')).done(function(r1, r2, r3, r4){
            set_position();
          });
       }, 
       function(){
        // actualizar tip con problemas en el servicio.
        update_tips( i18n.connection_error );
        return;
      });
   });
}

function add_contact_form_text(text) {
  $('#textoExplicacion').html(text);
  set_position()
}
        
function add_contact_thanks_form_text(text) {
  $('#textoAgradecimiento').html(text)
}

function create_contact_form(surveyCallback){
   // boton de envio de consulta
   $( document ).on("click", "#contact-button", function() {
     var contact_terms = $($( '#contact' ).find('#contact_terms')).is(":checked");
     var valid = check_length( $( '#contact' ).find('#contact_email'), "email", 6, 80 );
     valid = valid && check_regexp( $( '#contact' ).find('#contact_email'), /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. user@example.com" );

     // chequear el mail es un formato valido.
     if (!valid){
        $($( '#contact' ).find('#contact_email')).addClass( "ui-state-error" );
        update_tips( "El campo de correo debe ser completado" );
        return;
     } else {
        $($( '#contact' ).find('#contact_email')).removeClass( "ui-state-error" );
     }

     // nombre deben ser completado
     valid = check_length( $( '#contact' ).find('#contact_name'), "text", 6, 80 );
     if (!valid){
        $($( '#contact' ).find('#contact_name')).addClass( "ui-state-error" );
        update_tips( "El campo de nombre debe ser completado" );
        return;
     } else {
        $($( '#contact' ).find('#contact_name')).removeClass( "ui-state-error" );
     }

     // comentarios deben ser completado
     valid = check_length( $( '#contact' ).find('#contact_comments'), "text", 6, 80 );
     if (!valid){
        $($( '#contact' ).find('#contact_comments')).addClass( "ui-state-error" );
        update_tips( "El campo de comentarios debe ser completado" );
        return;
     } else {
        $($( '#contact' ).find('#contact_comments')).removeClass( "ui-state-error" );
     }

     // chequeado el terms?
     if(!contact_terms) {
        $($( '#contact' ).find('#contact_terms')).addClass( "ui-state-error" );
        update_tips( i18n.privacy_policy );
        return;
     } else {
        $($( '#contact' ).find('#contact_terms')).removeClass( "ui-state-error" );
     }
     var contact_email = $( '#contact' ).find('#contact_email').val();
     var contact_name = $( '#contact' ).find('#contact_name').val();
     
     var contact_comments = $( '#contact' ).find('#contact_comments').val();
     
     surveyCallback(contact_name, contact_email, contact_comments);

   });
   set_position();
}