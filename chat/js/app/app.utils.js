function cache_buster(url){
	if (url.indexOf('?') > -1)
		return url + '&rnd=' + Math.random();
	return url + '?rnd=' + Math.random();
}

//Clean text received
function clean_msg ( text ) {
  //Clean html tags
  var msg_html = text.replace( /(<([^>]+)>)/ig,"" );
  //Clean html tabs and new line
  var msg_done = msg_html.replace(/(\n|\r|\r\n)$/, '');
  return msg_done;
}

 function standard_date(date) {
     return moment(date).format('YYYY-MM-DD HH:mm');
 }

// valida que el mail sea de formato valido
function validate_email(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

// Agrega 0 luego del digito si < 10
function add_zero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

// Reduce el texto a 8 caracteres
function clean_name( text ) {
    var text_arr = text.split(" ");
    if ( text_arr[0].length > 9 )
      text_arr[0] = text_arr[0].substr(0, 8) + "..";
    
    return text_arr[0]
}

// Formatea la fecha mostrando en dos digitos la hora: HH:mm
function convert_date(stringdate){
  var DateResult;
  var StringDateResult = "";
  console.log(stringdate);
  DateResult = moment(stringdate).toDate();
  // format the date properly for viewing
  StringDateResult = (add_zero(DateResult.getHours()))+':'+(add_zero(DateResult.getMinutes()));

  return StringDateResult;
}

function new_scroll(s) {
  var sint = s.prop('scrollHeight') + 'px';

  s.slimScroll({
    size: '5px',
    scrollTo : sint,
    height: '173px',
    alwaysVisible: true,
    disableFadeOut : true,
    start: 'bottom'
  });
}

function set_widget_position_over_icon (widget, collision) {
  widget.dialog( "option", "position", { my: "right bottom", at: "right top-25%", of: "#chat_status", collision: collision });
}


function check_regexp( o, regexp, n ) {
  if ( !( regexp.test( o.val() ) ) ) {
    o.addClass( "ui-state-error" );
    update_tips( n );
    return false;
  } else {
    return true;
  }
}

function check_length( o, n, min, max ) {
    if ( o.val().length > max || o.val().length < min ) {
      o.addClass( "ui-state-error" );
      update_tips( i18n.length_of +" " + n + " "+ i18n.must_be_between + " " +
        min + " " + i18n.and + " " + max + "." );
      return false;
    } else {
      return true;
    }
}

function update_tips( t ) {
  var tips = $( ".validateTips" );
  tips
    .text( t )
    .addClass( "ui-state-highlight" );
  tips.show();
  set_position();
}

  //Change position on resize window
  function set_position() {
    //Main chat
    var resizer = $( "#main-users-resizer" );
    if (resizer)
      resizer.position({ my: "right bottom", at: "right top-25%", of: "#chat_status", collision: "flip, none" });
  }
  
  function show(widget) {
    if ($(widget).hasClass('no-display')===true) { 
      $(widget).removeClass('no-display'); 
      set_position();
    }
  }

  function hide(widget) {
    if ($(widget).hasClass('no-display')===false) {
      $(widget).addClass('no-display');
      set_position()
    } 
  }
  
  function hide_widgets(){
    for (var element in widgets_list){
        hide($('#extras').children(widgets_list[element]))
    }
  }

  function set_offline() {
    user_name = null;
    fin_chat = false;
    reset_unread_messages();
    remove_bounce();
    session_started = false;
    setTimeout(function () {
        main_chat_status( i18n.disconnected, 'offline' );
      }, 700);
  }

function set_online() {
  setTimeout(function () {
      session_started = true;
      main_chat_title();
      main_chat_status( i18n.connected, 'online' );
    }, 700);
}