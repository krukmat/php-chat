
function startup_login(data) {

    if ( send_to_ia === false){
        // Pooling cada 5"
        show_main_window();
        assign_notificator(function(){init_ping(session_id)}, 5000);
    } else {
        // Watson =>
        if (data){
          agent_id = data['response']['AgentId'];
          agent_name = data['response']['AgentNickName'];
          var is_typing = data['response']['IsTyping'];
          var text = data['response']['Text'];
          new_operator_message(agent_id, agent_name, is_typing, text, true);
        } else {
          // se muestra solamente la ventana. Despues deberia reconstruirse el historial de chat
          show_chat(agent_id, agent_name, session_id, false);
          $(window).trigger('resize');
        }
    }
  }

  function do_login() {
      console.log('session_id');
      console.log(session_id);
      if (!session_id) {
        // user_name
        // user_email
        post_login( user_name, user_email, function(data) {
            console.log(data);
            // chequeo si tengo que mostrar el formulario de contacto
            if (data.show_contact_form){
              contact_form_text = data.show_contact_form_message;
              show_contact_form();
              return;
            }
            startup_login(data);
        }, function(data){
            update_tips( i18n.connection_error );
            // En el caso que no se pueda hacer login, mostrar un mensaje en la ventana.
            console.log(data);
        });
      } else {
        // En el caso de restaurar sesion y es watson => no haria nada...
        startup_login();
      }
      
  }
  

function valid_login_inputs(name, email, terms, validateFunction, postValidateCallback) {

      if(!terms.is(":checked")) {
          terms.addClass( "ui-state-error" );
          update_tips( i18n.privacy_policy );
      }else{
          if ( validateFunction() ) {
              name.removeClass( "ui-state-error" );
              email.removeClass( "ui-state-error" );
              terms.removeClass("ui-state-error");
              $( ".validateTips" ).hide();
              postValidateCallback();

              //Open chat
              hide('#login');
              hide_widgets();
              show_waiting_agent_form();
              hide('#users');
             
              main_chat_init_session();
          }
      }
}

  function validate_login(name, email, terms){
      // should check the terms
      valid_login_inputs(name, email, terms, function() {
          var bValid = true;
          // TODO:Este es en el caso de cerrarlo y querer volver a abrirlo
          bValid = bValid && check_length( name, "username", 3, 16 );
          bValid = bValid && check_length( email, "email", 6, 80 );

          bValid = bValid && check_regexp( name, /^[a-z]([0-9a-z_\ ])+$/i, i18n.validate_username );
          // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
          bValid = bValid && check_regexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. user@example.com" );
          return bValid;
      }, function() {
          user_email = email.val();
          user_name  = name.val();
      });
  }

  function do_login_setup()
  {
      if (is_minimized) {
          restore_chat_window();
      } else {
          if ((!user_name || !user_email) && (!session_id)){
              show_login(chat_title_status);
          } else {
              hide('#login');
              show_waiting_agent_form();
              dialog_close();
              do_login();
          } 
      }
  }

  function show_login(chat_title_status) {
    show('#login');
    main_chat_title( );
    $( "#main-users-resizer" ).toggle();
    //Set the position every time chat button is clicked
    set_position();
  }