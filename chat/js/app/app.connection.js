
function get_ping(session_id, finishedConnectionCallback, waitingCallback, onlineCallback, errorCallback) {
	$.get(api_chat(session_id)).done(function(data){
            console.log(data);

            // Llego un error
            if (data.error){
                errorCallback(data);
            }
            
           // killed => Mostrar formulario de satisfaccion. fin
            if (data===3){
            	finishedConnectionCallback(data);
            }

            // estado de espera
            if (data === 2){
            	waitingCallback(data);
            }

            // en el caso de online => levantar 
            if (typeof data === 'object') {
                agent_id = data['AgentId'];
                agent_name = data['AgentNickName'];
                is_typing = data['IsTyping'];
                msg = data['Text'];
                onlineCallback(agent_id, agent_name, is_typing, msg);
            }
  
        }).error( function(data){
            console.log(data);
            // Mostrar mensaje de error y cerrar la sesion. Mostrar login con un mensaje
            errorCallback(data);
        });
}


function get_restore_session(session_id, doneOKCallback, errorCallback) {
	return $.get(api_restore_session(session_id)).done(function(data) {
                  console.log(data);
                  if (data.show_contact_form === false) {
                    send_to_ia = data.send_to_ia;
                    // En el caso de Watson => El nick y el id se devuelve en el login
                    if (send_to_ia){
                      agent_name = data.response.AgentNickName;
                      agent_id = data.response.AgentId;
                    }
                    complete_chat_history = data.response.ChatHistory;
                    // Inicializo sesion
                    doneOKCallback();
                  } else {
                    // si paso mucho tiempo y la sesion paso a estar fuera dl horario se debe destruir
                    contact_form_text = data.show_contact_form_message;
                    destroy_session();
                    errorCallback();
                  }

              }).error(function(){
                  destroy_session();
                  errorCallback();
              });
}

function get_customercareschedule(doneOKCallback, errorCallback){
	return $.get(api_customercareschedule()).done(doneOKCallback).error(errorCallback);
}

function post_login(user_name, user_email, doneOKCallback, errorCallback){
  current_object = {'user_name': user_name, 'user_email': user_email};

  console.log(current_object);

  $.post(api_login(), current_object).done( function(data) {
      console.log(data);
      
      session_id = data.session_id;
      send_to_ia = data.send_to_ia;

      save_session(session_id);
      doneOKCallback(data);
  }).error( function(data){
      errorCallback(data);
  });
}

function delete_chat_session(session_id) {
  $.ajax({
      url: api_chat(session_id, 'kill'),
      type: 'DELETE',
      success: function(result) {
         console.log(result);
      },
      error: function(result){
          console.log(result);
      }
  });
  destroy_session();
}

function post_survey(session_id, survey_mm, survey_chat, survey_comments) {

	var http_object = {
        'survey_mm': survey_mm,
        'survey_chat': survey_chat,
        'survey_comments': survey_comments,
        'session_id': session_id
      };

      //envio de survey
      $.post( api_survey(), http_object).done(function(data) {
            console.log(data);
      });
}

function post_contact(contact_name, contact_email, contact_comments, doneOKCallback, errorCallback){   
	var http_object = { 'contact_name': contact_name, 'contact_email': contact_email, 'contact_comments': contact_comments};
	$.post(api_contact(), http_object).done(doneOKCallback).error(errorCallback);
}

// Envia el evento que el usuario esta escribiendo
function do_is_writing(session_id, errorCallback) {
	$.ajax({
	    url: api_chat(session_id),
	    type: 'PUT',
	    success: function(result) {
	       console.log(result);
	    },
	    error: function(result){
	        console.log(result);
          errorCallback(result);
	    }
	});
}

// Envia el evento que el usuario finalizo de escribir
function do_is_done_writing(session_id, errorCallback) {
	$.ajax({
  	url: api_chat(session_id, 'userwriting'),
  	type: 'DELETE',
  	success: function(result) {
  	   console.log(result);
  	},
  	error: function(result){
  	    errorCallback(result);
  	}
	});
}

function post_messsage(session_id, text, redirectEmergiaCallback, regularMessageCallback, errorCallback)
{ 
  // En el caso de Watson => enviar el like
  if (send_to_ia)
    current_object = {'session_id': session_id, 'text': text, 'like': like};
  else
    current_object = {'session_id': session_id, 'text': text};

    console.log(current_object);

    $.post(api_chat(session_id), current_object).done( function(data) {
        if (data !== null)
          {
            console.log(data);
            fin_chat = data.fin_chat;
            if (send_to_ia) {
                // en el caso de post y es watson => la respuesta es automatica
                if (data.redirect_emergia === true) {
                    // watson termino o fallo la API => setear ping. cerrar actual ventana de chat
                  redirectEmergiaCallback(data)
                }else
                  regularMessageCallback(data);
            }
          } else {
            errorCallback(data);
          }
    }).error( function(data) {
        console.log(data);
        errorCallback(data);
    });
}