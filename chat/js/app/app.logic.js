var chat_stat = 0,
  chat_reconnect = 0,
  chat_title_status = 2,
  chat_changed_lang = true,
  user_email,
  user_name,
  user_avatar,
  socket;
  
  var like = true; // feedback a watson de la respuesta
  var main; //chat dialog
  
  var is_minimized = false;

  //TODO: session_started: Reemplazarlo por session_id?
  var session_started = false;
  var unread_messages = 0;
  var restored_session = false;
  var fin_chat = false;
  
  var widgets_list = ['#survey', '#waiting_agent', '#contact_thanks', '#contact', '#chat'];

  var conf_domain,
  conf_server,
  conf_protocol,
  conf_port,
  conf_debug,
  conf_lang_default,
  conf_lang_text,
  conf_lang_i18n,
  agent_name,
  is_typing,
  response,
  session_id,
  send_to_ia,
  agent_name,
  agent_id,
  complete_chat_history,
  contact_form_text,
  data;

  function restore_chat_history(chat_history) {
      console.log(chat_history);
      $(chat_history).each(function(chat) {
        // si origin es mail => usuario sino operador
        chat = chat_history[chat];
        var chat_window   = current_chat();
        var chat_text = '';
        // En el caso de watson devuelve un array. Podria manejarse desde el backend...
        console.log(chat);
        if ($.isArray(chat.text))
          chat_text = chat.text.join(' ');
        else
          chat_text = chat.text;
        console.log(chat.date);
        // en el caso del usuario se guarda como id en el historial de chat
        if (validate_email( chat.origin )) {
          // en este caso es un chat del usuario
          append_msg_me( chat_text, chat_window, chat.date);
        } else {
          append_msg_he( chat_text, chat_window, agent_name, chat.date );
        }
         set_position();
      })
  }

  function check_session(restore) {
      session_id = get_session();
      if (session_id != null) {
          // hay sesion recupero el historial con el primer ping
          get_restore_session(session_id, function() {
            restored_session = true;
            set_online();
            do_check_availability();
          }, function() {
              // mostrar el formulario de contacto ante cualquier eventualidad
              console.log('check_session');
              set_offline();
          });
      }
  }
      
  //Disconnect
  function main_chat_close() {

    main_chat_title();

    release_notificator();
    user_name = '';
    user_email = '';

    dialog_close();
    remove_bounce();
    chat_clean();

    hide_widgets();
    show('#login');

  }
  
  function main_set_conf () {
    try {

      if( !conf ) {
        alert( "error, conf not exist" );
        return true;
      }

      conf_domain = conf["domain"];
      conf_protocol = conf["protocol"];
      conf_server = conf["server"];
      conf_port = conf["port"];
      conf_debug = conf["debug"];
      conf_lang_default = conf["lang_default"];
      conf_lang_text = new Array();
      conf_lang_i18n = new Array();

    } catch( error ) {
      alert( error );
      return true;
    }

  }

  // Enlace con el backend ping
  function chat_ping(session_id){
      get_ping(session_id, function() {
          // finished
          show_survey_form(session_id);
      }, 
      function() {
        // waiting
      },
      function(_agent_id, _agent_name, _is_typing, _msg) {
            // online
            agent_id = _agent_id;
            agent_name = _agent_name;
            is_typing = _is_typing;
            msg = $.trim(_msg);
            do_is_typing(is_typing);
            is_minimized = !current_chat().parent().is(':visible');
            if (msg.length > 0) {
              // rebota si esta minimizado
              console.log(is_minimized);
              if (is_minimized){ 
                add_bounce();
                unread_messages += 1;
                add_unread_messages(unread_messages);
              }
              agent_new_chat(msg);
            }
      }, function(){
          //error
          set_offline();
          kill_chat(session_id);
          destroy_session();
          update_tips( i18n.connection_error );
      });
  }

  function init_ping(session_id){

      get_ping(session_id, function() {
          // finished. destruir la sesion
          release_notificator();
          set_offline();
          destroy_session();
          // login nuevamente
          hide('#waiting_agent');
          show('#login');
      }, 
      function() {
        //waiting
        if (!$('#waiting_agent').is(':visible'))
            show('#waiting_agent');
      },
      function(_agent_id, _agent_name, _is_typing, _msg) {
            //online
            agent_id = _agent_id;
            agent_name = _agent_name;
            is_typing = _is_typing;
            response = _msg;
            release_notificator();
            show_chat(agent_id, agent_name, session_id, true);
            agent_new_chat(response);
            $(window).resize();
      }, function() {
          //error
          release_notificator();
          set_offline();
          destroy_session();
      });
  }

  function do_post_message(text) {
      post_messsage(session_id, text, function(data) {
        // redirectEmergia flow
        send_to_ia = false;
        fin_chat = false;
        // envio el ultimo mensaje al chat
        agent_id = data['response']['AgentId'];
        agent_name = data['response']['AgentNickName'];
        var is_typing = data['response']['IsTyping'];
        var text = data['response']['Text'];
        new_operator_message(agent_id, agent_name, is_typing, text, false);
        // llamo al init ping
        setTimeout(function(){
            // cierro el dialogo del chat des pues de 3''
            toggle_chat_dialog(true);
            restored_session = true;
            show('#waiting_agent');
            // inicio al segundo el init ping
            assign_notificator(function(){
              init_ping(session_id);
            }, 1000);
        }, 3000)



      }, function(data){
          // regularMessage
          if (data) {
            agent_id = data['response']['AgentId'];
            agent_name = data['response']['AgentNickName'];
            var is_typing = data['response']['IsTyping'];
            var text = data['response']['Text'];
            new_operator_message(agent_id, agent_name, is_typing, text, false);
            if (fin_chat) {
              console.log('do_post_message.show_survey_form');
              // despues de 3" muestra el formulario de encuesta
              assign_notificator(function(){
                show_survey_form(session_id);
                release_notificator();
              }, 3000);
            }
        } else {
          update_tips( i18n.connection_error );
          disable_chat_input();
        }
      }, function(){
          // error
          update_tips( i18n.connection_error );
          disable_chat_input();
      });
  }

  function saveLike(index){
    var option = $("#like_" + index);
    like = option.attr('title');
  }


  // Esta funcion es llamada desde el mismo codigo que  envia watson al mandar el catalogo de productos
  function saveProduct(index){
    
    if(index ==0){
      do_post_message('el primero');
    }
    
    if(index ==1){
      do_post_message('el segundo');
    }
    
    if(index ==2){
      do_post_message('el tercero');
    }
    
    if(index ==3){
      do_post_message('el cuarto');
    }
    
    if(index ==4){
      do_post_message('el quinto');
    }

  }

  function do_check_availability(){ 
    get_customercareschedule( function(data) {
          hide_widgets();
          if (data.show_contact_form === false){
              reset_unread_messages();
              if ((!restored_session) || (send_to_ia))
                do_login_setup();
              else{
                if (!send_to_ia)
                  assign_notificator(function(){init_ping(session_id)}, 1000);
              }
          } else {
              // mostrar el formulario de contacto
              console.log('do_check_availability');
              contact_form_text = data.show_contact_form_message;
              main_window_toggle();
              show_contact_form();
              return;
          }
      }, function(data) {
          hide_widgets();
          console.log(data);
          console.log('do_check_availability');
          // Mostrar el formulario de contacto si no funciona la API
          main_window_toggle();
          show_contact_form();
      });
  }

  function kill_chat(session_id){
      release_notificator();
      delete_chat_session(session_id);
  }
  
