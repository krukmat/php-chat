// A 100% javascript facebook/gmail style web chat.
// (c) 2014-2015 Ezequiel Lovelle [ezequiellovelle@gmail.com]
// (c) 2016 Matias Kruk [kruk.matias@amaris.com]
(function( $ ) {
  
  var xhr_toolbar = $.get( cache_buster("views/toolbar.html"), function( toolbar ) {
    $( "body" ).append( toolbar );
  });
  
  var xhr_mchat = $.get( cache_buster("views/main-chat.html"), function( main_chat ) {
    $( "body" ).append( main_chat );
  });
  
  var xhr_closing_confirm_dialog = $.get( cache_buster("views/closing-confirm-dialog.html") , function( closing_confirm_dialog ) {
      $( "body" ).append( closing_confirm_dialog );
  });

  $.when( xhr_mchat, xhr_toolbar, xhr_closing_confirm_dialog).done(function(r1, r2, r3) {
    
    var xhr_contact = $.get( cache_buster("views/contact_form.html"), function( contact ) {
      $( "#extras" ).append( contact );
    });
    var xhr_login = $.get( cache_buster("views/dialog-login.html"), function( dialog_login ) {
      $( "#extras" ).append( dialog_login );
    });
    var xhr_contact_thanks = $.get( cache_buster("views/contact-thanks.html"), function( contact_thanks ) {
      $( "#extras" ).append( contact_thanks );
    });
    
    var xhr_waiting_agent = $.get( cache_buster("views/waiting-agent.html"), function( waiting_agent ) {
      $( "#extras" ).append( waiting_agent );
    });
    
    var xhr_chat = $.get( cache_buster("views/chat.html"), function( chat ) {
      $( "#extras" ).append( chat );
    });
    

    $.when(xhr_contact, xhr_login, xhr_contact_thanks, xhr_waiting_agent).done(function(r3, r4, r5, r6) { 
      main_set_conf();
      main_set_i18n("");
      
      //Main chat title bar
      main_chat_status( i18n.disconnected, "offline" );
        
      $(document).on("click", "#login-button", function() {
          // validar valores de usuario y correo y checkbox
          var name = $(this).parent().find('#name');
          var email = $(this).parent().find('#email');
          var terms = $(this).parent().find('#terms');
          validate_login(name, email, terms);
      });
  
      $( "#main-users-resizer" ).resizable({
        handles: "n, w",
        minHeight: 200,
        minWidth: 200,
  
        create: function() { set_position( ); },
        resize: function() { set_position();
                           }
      });
      
      $( window ).resize(function() {
        set_position();
      });
  
      //Main hide
      $( "#main-users-resizer" ).hide();
  
      $( document ).on("click", "#chat-title-button", function() {
        remove_bounce();
        main_chat_init_session();
      });
      
      $( document ).on("click", "#close-main-chat", function() {
        $( "#main-users-resizer" ).hide();
        main_chat_close();
        set_offline();
        destroy_session();
      });
  
      //Minus icon in main chat
      $( document ).on("click", "#min-main-chat", function() {
          main_chat_minimize();
      });
      
      //Close chat's window
      $( document ).on("click", "#close-chat", function() {
        remove_bounce();
        main_chat_shutdown();
      });

      //iniciar sesion guardada si la hay
      check_session(false);
      document.getElementById('main').style.display = 'block';

    }).fail(function() {
       console.log('chat or contact failure');
    })

  }).fail(function() {
    console.log('fail');
  });

}( jQuery ));
