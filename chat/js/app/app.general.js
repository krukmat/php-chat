
function main_chat_status ( text , status ) {
    if ( status == "online" ) {
      $( "#chat_status" ).addClass('active_chat');
      $( "#chat_status" ).removeClass('inactive_chat');
    }
    else {
      $( "#chat_status" ).addClass('inactive_chat');
      $( "#chat_status" ).removeClass('active_chat');
    }
    set_position();
}

function main_chat_init_session(){
  // chequear el horario de atencion solo si no hay sesion definida
  if ((!session_id))
    do_check_availability();
  else
    // si la sesion esta definida => es la app que esta minimizada o maximizada => minimizar o maximizar
    main_chat_minimize();
}

function main_chat_shutdown(){
      if (session_started) {
        closing_confirm_dialog();
      } else {
        $( "#main-users-resizer" ).hide();
        // icono de chat a offline
        show_survey_form(session_id);
      }
}

function main_chat_minimize(){
  if (current_chat()) {
          reset_unread_messages();
          current_chat().parent().toggle();
          is_minimized = !current_chat().parent().is(':visible');
          var scroll = $('.direct-chat-messages').parent();
          new_scroll(scroll);
          set_widget_position_over_icon(current_chat(),  "flip, none");
  } else {
       $( "#main-users-resizer" ).toggle();
       is_minimized = !$( "#main-users-resizer" ).is(':visible');
  }
}

function dialog_close() {
  $( ".ui-dialog-content" ).dialog( "close" );
}

function main_chat_title(  ) {

        var text;
        var label;
  
        //Reset in changed chat
        if ( chat_changed_lang == true )
          $( "#chat-main-title-ui" ).remove();
  
        text  = i18n.title;
        label = i18n.no_users;
  
        if ( $('#chat-title').find('#chat-main-title-ui').length == 0 )
            $( "#chat-title" ).append( "<div id='chat-main-title-ui' class='ui-widget'><div id='chat-main-title-id' class='chat-main-title'>" + text + "</div></div>" );
  
        change_chat_title(text);

        set_position();
}

  
  function show_waiting_agent_form() {
    $('#waiting_agent').parent().find('#waiting_agent_line1').html(i18n.waiting_agent_line1); 
    $('#waiting_agent').parent().find('#waiting_agent_line2').html(i18n.waiting_agent_line2);
    show('#waiting_agent');
    $(window).resize();
  }

function show_main_window() {
  if (!$( "#main-users-resizer" ).is(':visible'))
      $( "#main-users-resizer" ).toggle();
}

function main_set_i18n (text_status) {
    try {
      if( !i18n ) {
        alert( "Error, i18n not exist" );
        return true;
      }

      var i18n_elem = ['chat', 'expand', 'collapse', 'options', 'loading', 'connected', 'disconnected',
      'login', 'name', 'email', 'and', 'minimize', 'close',
      'char_max', 'is_writing', 'lang', 'no_users', 'seconds', 
      'length_of', 'must_be_between', 'validate_username', 'new_messages', 'type_message' ];

      for (var i = 0; i < i18n_elem.length; i++) {
        if (i18n[i18n_elem[i]] === undefined || i18n[i18n_elem[i]] === null) {
          alert( "Error, element i18n_" + i18n_elem[i] + " is undefined or null" );
          return true;
        }
      }

    } catch( error ) {
      alert( error );
      return true;
    }

    $( "#textarea_msg" ).attr( "placeholder", i18n.type_message );
    $( "#min-main-chat" ).attr( "title", i18n.minimize );
    
    $( "#lang" ).text( i18n.lang );
    
    main_chat_title( );
    $( "#login" ).attr( "title", i18n.login );

    $( "#login" ).find( "label#label_name" ).text( i18n.name );
    $( "#login" ).find( "label#label_email" ).text( i18n.email );

}

function restore_chat_window() {
  reset_unread_messages()
  main_chat_minimize();
}

function main_window_toggle() {
  $( "#main-users-resizer" ).toggle();
}

function main_window_hide() {
  $( "#main-users-resizer" ).hide();
}

