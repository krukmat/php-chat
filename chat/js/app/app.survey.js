function setup_survey_slider(survey_comp, survey_slider, label) {
  
  var slider = $(document).find(survey_slider).bootstrapSlider({
      formatter: function(value) {
        if (value === -1)
          return "NS-NC";
        return value;
      }
  });

  slider.bootstrapSlider('setValue', -1);
  $(label).text("NS-NC");

  $(document).find(survey_slider).on("slide", function(slideEvt) {
      if (slideEvt.value == -1)
        $(label).text("NS-NC");
      else {
        // 0-4: rojo
        if (slideEvt.value < 5) 
          $(this).find('.slider-selection').css('background', 'red');
        else {
            //5-7: amarillo
            if ( (slideEvt.value > 4) && (slideEvt.value < 8))
              $(this).find('.slider-selection').css('background', 'yellow');
            else 
              // 8-10: verde
              $(this).find('.slider-selection').css('background', 'green');
        }
        $(label).text(slideEvt.value);
      }
  });
}

function init_surveys_ie8_compatibility(survey) {
  var ie_compat = false;
   try {
        setup_survey_slider(survey, '#recommendMediaMarkt', "#label-recommendMediaMarkt");
        setup_survey_slider(survey, '#recommendChat', "#label-recommendChat");
  } 
  catch(e) {
        console.log('error IE-8');
        ie_compat = true;

        $.fn.switchToSelect = function(newType) {
            var attrs = {};

            $.each(this[0].attributes, function(idx, attr) {
                attrs[attr.nodeName] = attr.nodeValue;
            });

            // Como IE-8 no carga el slider => se carga un listbox con los mismos valores
            this.replaceWith(function() {
                var selectBox =  $("<select/>", attrs).append($(this).contents());
                var slider_min = parseInt(attrs['data-slider-min']);
                var slider_max = parseInt(attrs['data-slider-max']);
                var slider_step = parseInt(attrs['data-slider-step']);
                var slider_value = parseInt(attrs['data-slider-value']);

                for (var i=slider_min; i <= slider_max; i = i + slider_step){
                  if (i === -1) {
                    selectBox.append('<option selected value=' + i + '>NS-NC</option>');
                  } else
                    selectBox.append('<option value=' + i + '>' + i + '</option>');
                }
                return selectBox;
            });
        }
        // con el objeto hay: data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="5"
        // armar la lista de valores posibles
        $(document).find('#recommendMediaMarkt').switchToSelect();
        $(document).find('#recommendChat').switchToSelect();
    }
    return ie_compat;
}

function create_survey_close_button(survey, ie_8_mode, callBackSurveySend) {
  var survey_mm;
  var survey_chat;
  // Si esta en modo ie_8 => es el selected option de select sino es el valor de scale-xxx
  if (!ie_8_mode){          
      survey_mm = $(document).find('#label-recommendMediaMarkt').text();
      survey_chat = $(document).find('#label-recommendChat').text();
  } else {
      survey_mm = $(document).find('#recommendMediaMarkt').find("option:selected").text();
      survey_chat = $(document).find('#recommendChat').find("option:selected").text();
  }
  var survey_comments =  $(document).find('#suggestions').val();

  // envio al backoffice
  callBackSurveySend(survey_mm, survey_chat, survey_comments);
  console.log(survey_mm, survey_chat, survey_comments);

  hide('#survey');
  show($('#login'));

  // limpiar el historial de chat aqui
  main_window_hide();
}

function create_survey_sliders(survey, callBackSurveySend) {
  // TODO: Refactorear esto
    var ie_8_mode = init_surveys_ie8_compatibility(survey);
    
    // button to send and close the survey. Siempre enviar la data? 
    $(document).find('#close-survey-button').on("click", function(){
        create_survey_close_button(survey, ie_8_mode, callBackSurveySend);
    });
}

function show_survey_form(session_id) {
    
    set_offline();
    chat_clean();
    
    hide('#login');
    hide_widgets();
    
    //dialog_close();
    toggle_chat_dialog(false);
    main_window_toggle();
    $( "#main-users-resizer" ).show();
    
    
    if (session_id) {       
       change_chat_title(i18n.survey_title);
       // template del survey
       kill_chat(session_id);
       var xhr_survey = $.get( cache_buster("views/survey.html"), function( survey ) {
        $( "#extras" ).append( survey );
        create_survey_sliders(survey, function(survey_mm, survey_chat, survey_comments) {
          post_survey(session_id, survey_mm, survey_chat, survey_comments);
          // kill the chat
          destroy_session();
          $('#survey').remove();
          show('#login');
        }, function(){
            destroy_session();
            $('#survey').remove();
        });
        show('#survey');
      });
    }
}