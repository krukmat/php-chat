function supports_html5_session_storage() {
  try {
    return 'sessionStorage' in window && window['sessionStorage'] !== null;
  } catch (e) {
    return false;
  }
}

function create_cookie(session_id) { 
  var now = new Date();
  var time = now.getTime();
  var expireTime = time + 1000*36000;
  now.setTime(expireTime);
  document.cookie = 'session_id='+session_id+';expires='+now.toGMTString()+';path=/';
}

function delete_cookie(name) {
  document.cookie = name+'=; expires=1970-01-01 00:00:01 GMT;path=/';
}

function save_session(session_id){
	if (supports_html5_session_storage()) {
		window.sessionStorage.setItem('session_id', session_id);
	} else {
		create_cookie(session_id);
	}
}

function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return null;
}

function get_session(){
	if (supports_html5_session_storage()) {
		return window.sessionStorage.getItem('session_id');
	} else {
		return get_cookie('session_id');
	}
}

function destroy_session()
{
  if (supports_html5_session_storage()) {
    window.sessionStorage.removeItem('session_id');
  } else {
    delete_cookie('session_id');
  }
  session_id = null;
}