var ping_var;

function release_notificator(){
	clearInterval(ping_var);
}

function assign_notificator(callbackHandler, timeOut){
	ping_var = setInterval(function(){callbackHandler()}, timeOut);
}

// rebote del icono al recibir mensaje

function remove_bounce() {
  $(document).find('.label-important').remove();
  $(document).find('#chat-title-button').removeClass('animated bounce infinite');
}

function add_bounce() {
  $(document).find('#chat-title-button').addClass('animated bounce infinite');
}

function reset_unread_messages() {
  unread_messages = 0;
  $(document).find('.label-important').remove();
}

function add_unread_messages(number) {
  $(document).find('.label-important').remove();
  $(document).find('#chat-title-button').append('<span class="label label-important">'+parseInt(number)+'</span>');
}