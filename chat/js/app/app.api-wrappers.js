function api_chat(session_id, operation){
	var uri = conf_protocol + '://' + conf_server + ':' + conf_port + "/chatapi?session_id="+session_id;
	if (operation !== undefined)
		uri += '&operation=' + operation;
	return cache_buster(uri);
}

function api_login(){
	return cache_buster(conf_protocol + '://' +  conf_server + ':' + conf_port + "/login");
}

function api_customercareschedule(){
	return cache_buster(conf_protocol + '://' +  conf_server + ':' + conf_port + "/customercareschedule");
}

function api_restore_session(session_id){
	return cache_buster(conf_protocol + '://' +  conf_server + ':' + conf_port + "/login?session_id="+session_id);
}

function api_survey(){
	return cache_buster(conf_protocol + '://' +  conf_server + ':' + conf_port + "/survey");
}

function api_contact(){
	return cache_buster(conf_protocol + '://' +  conf_server + ':' + conf_port + "/contact");
}