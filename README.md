# PHP+JQuery Chat #

This jquery chat interacts with a IBM-Watson bot service. The backend layer is built in PHP with a simple REST framework.


 Docker Usage 

```
Clean it up: docker rm $(docker ps -a -q)
Build: docker-compose build
Run: docker-compose up
Bash: docker exec -i -t ca5fb3bb410a /bin/bash
```

### Installation ###

Local Terminal:
```
    * docker-compose up
    * docker ps -> check the id
    * docker exec -i -t b1bce6a87fe0 /bin/bash
```
In browser: 
```
http://docker_ip/webapp/#!/ -> use user created in the step before.
```

### Who do I talk to? ###
* Matias Kruk <kruk.matias at gmail.com>