<?php

namespace Entities;

/**
 * Description of ChatSessionEntity
 */
class ChatSessionEntity extends \Core\Entity
{
    public $token;
    public $headers;
    public $send_to_ia;
    public $success_ia;
    public $ia_start;
    public $ia_end;
    public $cc_start;
    public $cc_end;
    public $emergia_session_token;
    public $emergia_chat_token;
    public $watson_conversation_id;
    public $chat_history;
    public $survey_mm;
    public $survey_chat;
    public $survey_comments;
    public $user_name;
    public $user_email;
    public $priority;
    public $watson_token;
    public $send_chat;

    
    /**
     * Constructor of the class
     * @author aguilellaj
     * @since 2015-09-25
     * 
     * @global \Core\Application $application
     * @param int $id
     */
    function __construct($id = '')
    {   
        $this->chat_history = [];

        if ($id != '') {
            $model = new \Model\ChatSessionModel();
            $model->LoadEntity($this, $id, 'token');
        }
    }

    public function assignValues($session_creds)
    {
        foreach ($session_creds as $key => $value) 
        {
            $this->$key = $value;
        }
    }

    public function appendToChatHistory($chat_record)
    {
        $this->chat_history[] = $chat_record;
    }

}
