<?php
namespace Model;


require_once dirname(__FILE__) .'/../core/entity.php';
require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';

require_once dirname(__FILE__) .'/../entities/customercarescheduleentity.php';
require_once dirname(__FILE__) .'/../model/customercareschedulemodel.php';

/**
 * Description of customercarescheduleentitytest Tests
 *
 * @author krukmat
 */
 
class CustomerCareScheduleEntityTest extends \Model\ModelTest
{
	public function setUp()
	{
		global $modelName, $model;
        
		$model = new \Model\CustomerCareScheduleModel();
		$modelName = $model->getModelName();  //It's used for delete the collection aftwerwards
		\Model\ModelTest::setUp();
	}

	public function testConstruct()
	{	
		global $model;

		$date_from =date('Y-m-d');
		$date_to = date("Y-m-d", strtotime(' +4 day'));
		$week_days = null;
		$time_noon_from = '9:00';
		$time_noon_to = '13:00';
		$time_night_from = '17:00';
		$time_night_to = '22:00';

		$entity = $model->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

		$session_id = $entity->token;
		
		$entity2 = new \Entities\CustomerCareScheduleEntity($session_id);
		$this->assertEquals($entity2->token, $entity->token);
	}
	
	public function testCheckTimeInIntervalComplete()
	{	
		$customerCareScheduleEntity = new \Entities\CustomerCareScheduleEntity();
		// 9:00 - 13:00 y 17:00 a 23:00
		$customerCareScheduleEntity->time_noon_from = '09:00';
		$customerCareScheduleEntity->time_noon_to = '13:00';

		$customerCareScheduleEntity->time_night_from = '17:00';
		$customerCareScheduleEntity->time_night_to = '23:00';

		//chequeo las 10:00
		$this->assertTrue($customerCareScheduleEntity->checkTimeInInterval('10:00'));
		// chequeo las 7:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('7:00'));
		//chequeo 19:00
		$this->assertTrue($customerCareScheduleEntity->checkTimeInInterval('19:00'));
		//chequeo 0:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('0:00'));
	}


	public function testCheckTimeInIntervalOnlyNoon()
	{	
		$customerCareScheduleEntity = new \Entities\CustomerCareScheduleEntity();
		// 10:00 - 22:00. Agrego vacio en night para chequear que a las 0:00 no lo agregue
		$customerCareScheduleEntity->time_noon_from = '10:00';
		$customerCareScheduleEntity->time_noon_to = '22:00';

		$customerCareScheduleEntity->time_night_from = '';
		$customerCareScheduleEntity->time_night_to = '';


		//chequeo las 10:00
		$this->assertTrue($customerCareScheduleEntity->checkTimeInInterval('10:00'));
		// chequeo las 7:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('7:00'));
		//chequeo 19:00
		$this->assertTrue($customerCareScheduleEntity->checkTimeInInterval('19:00'));
		//chequeo 0:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('0:00'));
	}

	public function testCheckTimeInIntervalOnlyNight()
	{	
		$customerCareScheduleEntity = new \Entities\CustomerCareScheduleEntity();
		// 9:00 - 13:00 y 17:00 a 23:00

		$customerCareScheduleEntity->time_night_from = '17:00';
		$customerCareScheduleEntity->time_night_to = '23:00';

		//chequeo las 10:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('10:00'));
		// chequeo las 7:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('7:00'));
		//chequeo 19:00
		$this->assertTrue($customerCareScheduleEntity->checkTimeInInterval('19:00'));
		//chequeo 0:00
		$this->assertFalse($customerCareScheduleEntity->checkTimeInInterval('0:00'));
	}

}