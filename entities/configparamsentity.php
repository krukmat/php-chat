<?php

namespace Entities;

/**
 * Description of ConfigParamsEntity
 */
class ConfigParamsEntity extends \Core\Entity
{	
	public $token;
    public $watson_rate;
    public $message_agent_derivation;
    public $message_out_of_line;
}