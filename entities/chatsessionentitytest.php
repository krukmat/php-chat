<?php
namespace Entities;


require_once dirname(__FILE__) .'/../core/entity.php';
require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';

require_once dirname(__FILE__) .'/../entities/chatsessionentity.php';
require_once dirname(__FILE__) .'/../model/chatsessionmodel.php';

/**
 * Description of ChatSessionEntityTest Tests
 *
 * @author krukmat
 */
 
class ChatSessionEntityTest extends \Model\ModelTest
{
	public function setUp()
	{
		global $modelName;
        
		$chatSessionModel = new \Model\ChatSessionModel();
		$modelName = $chatSessionModel->getModelName();  //It's used for delete the collection aftwerwards
		\Model\ModelTest::setUp();
	}
	public function testConstruct()
	{
		$chatSessionModel = new \Model\ChatSessionModel();
		$entity = $chatSessionModel->createChatSession();
		$session_id = $entity->token;
		$entity2 = new ChatSessionEntity($session_id);
		$this->assertEquals($entity2->token, $entity->token);
	}

	public function testAssignValues()
	{
		$entity = new ChatSessionEntity();
		$session_creds = array('emergia_session_token' => '1234567','emergia_chat_token' =>'654321');
		$entity->assignValues($session_creds);

		$this->assertEquals($entity->emergia_session_token, '1234567');
		$this->assertEquals($entity->emergia_chat_token, '654321');
	}

}