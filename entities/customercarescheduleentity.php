<?php

namespace Entities;

/**
 * Description of customercarescheduleentity
 */
class CustomerCareScheduleEntity extends \Core\Entity
{
	public $date_from;
	public $date_to;
	public $week_days;
	public $time_noon_from;
	public $time_noon_to;
	public $time_night_from;
	public $time_night_to;
    public $token;


	/**
     * Constructor of the class
     * @author krukmat
     * @since 2016-11-07
     * 
     * @global \Core\Application $application
     * @param int $id
     */
    function __construct($id = '')
    {
        if ($id != '') {
            $model = new \Model\CustomerCareScheduleModel();
            $model->LoadEntity($this, $id, 'token');
        }
    }

    //@codeCoverageIgnoreStart
    public function readableDateFrom() 
    {
        return date('Y-m-d', $this->date_from->sec);
    }

    public function readableDateTo() 
    {
        return date('Y-m-d', $this->date_to->sec);
    }

    //@codeCoverageIgnoreEnd

    private function checkDateOrder($date_from, $date_to)
    {
            $date_from_date = new \DateTime($date_from);
            $date_to_date = new \DateTime($date_to);
            return ($date_from_date <= $date_to_date);
    }

    public function assignDates($date_from, $date_to)
    {
        if ((!is_null($date_from)) && (!is_null($date_to))) 
        {
            if ($this->checkDateOrder($date_from, $date_to))
            {
                $this->date_from = new \MongoDate(strtotime($date_from));
                $this->date_to = new \MongoDate(strtotime($date_to));
            } else {
                throw new \Exception('Invalid date');
            }
        }   
    }

    public function updateTimeField($time_field_from, $time_field_to, $time_from, $time_to)
    {
        if ((!is_null($time_from)) && (!is_null($time_to))) {
            if ($this->checkDateOrder($time_from, $time_to))
            {
                $this->$time_field_from = $time_from;
                $this->$time_field_to = $time_to;
                return true; 
            } else {
                return false;
            }
        }
        return true;
    }

    public function returnNoon($date) 
    {
            if (isset($this->time_noon_from) && isset($this->time_noon_to)) {
                            return array('time_noon_from' => $date."T".$this->time_noon_from.":00",
                                         'time_noon_to' => $date."T".$this->time_noon_to.":00"
                                         );
            }
            return null;
    }


    public function returnNight($date) 
    {
            if (isset($this->time_night_from) && isset($this->time_night_to)) 
            {
                return array('time_night_from' => $date."T".$this->time_night_from.":00",
                             'time_night_to' => $date."T".$this->time_night_to.":00",
                             );
            }

            return null;
    }

    private function checkTimeRange($time_from, $time_to, $current_hour, $current_minute)
    {
        if (isset($time_from) && isset($time_to) && strlen($time_from) > 0 && strlen($time_to) >0) {

            list($time_from_hour, $time_from_minute) = explode(':', $time_from);

            list($time_to_hour, $time_to_minute) = explode(':', $time_to);

            $from = intval($time_from_hour);
            $to = intval($time_to_hour);
            $int_current_hour = intval($current_hour);

            $range = range($from, $to-1);
            return in_array($int_current_hour, $range);
            //return ($from <= $int_current_hour) && ($to-1 >= $int_current_hour);
        }
        return false;
    }

    public function checkTimeInInterval($hour) 
    {
        //TODO: Solo estoy evaluando hora. Chequear minutos
        list($current_hour, $current_minute) = explode(':', $hour);
        //chequeo primero noon
        $check_noon = $this->checkTimeRange($this->time_noon_from, $this->time_noon_to, $current_hour, $current_minute);
        if ($check_noon)
            return true;
        // sino el resultado de night es el definitivo
        return $this->checkTimeRange($this->time_night_from, $this->time_night_to, $current_hour, $current_minute);
    }
}