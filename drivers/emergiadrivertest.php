<?php
namespace Drivers;


require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/entity.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';
require_once dirname(__FILE__) .'/../core/driver.php';

require_once dirname(__FILE__) .'/../apiclient/oauthclient.php';
require_once dirname(__FILE__) .'/../apiclient/emergiaapiclient.php';
require_once dirname(__FILE__) .'/../model/chatsessionmodel.php';
require_once dirname(__FILE__) .'/../entities/chatsessionentity.php';
require_once dirname(__FILE__) .'/emergiadriver.php';

/**
 * Description of EmergiaDriver Tests
 *
 * @author krukmat
 */
 
class EmergiaDriverTest extends \Model\ModelTest
{
	protected function setUp()
    {
        global $chatSessionModel, $modelName, $session_data;
        
		$chatSessionModel = new \Model\ChatSessionModel();
		$modelName = $chatSessionModel->getModelName();  //It's used for delete the collection aftwerwards
		\Model\ModelTest::setUp();
		$entity = $chatSessionModel->createChatSession();
		$session_id = $entity->token;
		$session_data = $chatSessionModel->getSessionData($session_id);
    }

	public function testInitOk()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->Token = "TokenId=GoX76Jn0E%252fHOYjmUlFgHQw%253d%253d&Issuer=&Expiration=0&HMACSHA256=pPbB0cQz1r5ToEXlwROznfapWQEtDTcTE%2fju4YU6%2b9s%3d";

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getToken')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));
		$mockEmergiaAPIClient->expects($this->any())->method('getChatSession')->will($this->returnValue("https://webchatesc.emergiacc.com/webchat/rest/api/v1/sessions/MjIwOjU4NA2"));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$response = $driver->init($chatSessionModel, $session_data);
		$session_data = $chatSessionModel->getSessionData($session_data->token);
		$this->assertEquals($session_data->emergia_session_token, "TokenId=GoX76Jn0E%252fHOYjmUlFgHQw%253d%253d&Issuer=&Expiration=0&HMACSHA256=pPbB0cQz1r5ToEXlwROznfapWQEtDTcTE%2fju4YU6%2b9s%3d");
		$this->assertEquals($session_data->emergia_chat_token, "https://webchatesc.emergiacc.com/webchat/rest/api/v1/sessions/MjIwOjU4NA2");
	}

	public function testInitOkContinueSession()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->Token = "TokenId=GoX76Jn0E%252fHOYjmUlFgHQw%253d%253d&Issuer=&Expiration=0&HMACSHA256=pPbB0cQz1r5ToEXlwROznfapWQEtDTcTE%2fju4YU6%2b9s%3d";

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->exactly(0))->method('getToken')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));
		$mockEmergiaAPIClient->expects($this->exactly(0))->method('getChatSession')->will($this->returnValue("https://webchatesc.emergiacc.com/webchat/rest/api/v1/sessions/MjIwOjU4NA2"));

		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => '12345', 'origin' => WatsonDriver::$bot_name);
		$chatSessionModel->updateChatHistory($session_data->token, $chat_record);

		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => '67890', 'origin' => 'blum');
		$chatSessionModel->updateChatHistory($session_data->token, $chat_record);
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$session_data = $chatSessionModel->getSessionData($session_data->token);
		$response = $driver->init($chatSessionModel, $session_data, 1, true);
		$this->assertEquals(count($response['response']['ChatHistory']), 2);
	}

	public function testPostCompleteChatHistory()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam', 'postMessage'));
		$mockEmergiaAPIClient->expects($this->exactly(1))->method('postMessage')->will($this->returnValue(true));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		// insertion of 2 records in chat register
		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => '12345', 'origin' => WatsonDriver::$bot_name);
		$chatSessionModel->updateChatHistory($session_data->token, $chat_record);

		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => '67890', 'origin' => 'blum');
		$chatSessionModel->updateChatHistory($session_data->token, $chat_record);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$driver->postCompleteChatHistory($chatSessionModel, $session_data);
	}

	public function testNoPostCompleteChatHistory()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam', 'postMessage'));
		$mockEmergiaAPIClient->expects($this->exactly(0))->method('postMessage')->will($this->returnValue(true));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$driver->postCompleteChatHistory($chatSessionModel, $session_data);
	}


	public function testInitErrorCode()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->Token = "TokenId=GoX76Jn0E%252fHOYjmUlFgHQw%253d%253d&Issuer=&Expiration=0&HMACSHA256=pPbB0cQz1r5ToEXlwROznfapWQEtDTcTE%2fju4YU6%2b9s%3d";

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getToken')->will($this->returnValue(array('Code' => -60, 'ErrorMessage' => 'Error')));
		$mockEmergiaAPIClient->expects($this->any())->method('getChatSession')->will($this->returnValue("https://webchatesc.emergiacc.com/webchat/rest/api/v1/sessions/MjIwOjU4NA2"));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$this->setExpectedException(EmergiaError::class);
		$driver->init($chatSessionModel, $session_data);
	}

	public function testInitEmptyToken()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->Token = "TokenId=GoX76Jn0E%252fHOYjmUlFgHQw%253d%253d&Issuer=&Expiration=0&HMACSHA256=pPbB0cQz1r5ToEXlwROznfapWQEtDTcTE%2fju4YU6%2b9s%3d";

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getToken')->will($this->returnValue(null));
		$mockEmergiaAPIClient->expects($this->any())->method('getChatSession')->will($this->returnValue("https://webchatesc.emergiacc.com/webchat/rest/api/v1/sessions/MjIwOjU4NA2"));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$this->setExpectedException(EmptyTokenError::class);
		$driver->init($chatSessionModel, $session_data);
	}

	public function testInitInvalidSession()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->Token = "TokenId=GoX76Jn0E%252fHOYjmUlFgHQw%253d%253d&Issuer=&Expiration=0&HMACSHA256=pPbB0cQz1r5ToEXlwROznfapWQEtDTcTE%2fju4YU6%2b9s%3d";

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getToken', 'getChatSession', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getToken')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));
		$mockEmergiaAPIClient->expects($this->any())->method('getChatSession')->will($this->returnValue(null));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$this->setExpectedException(InvalidSession::class);
		$driver->init($chatSessionModel, $session_data);
	}

	public function testPingOkWaiting()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->State = 2;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getSessionStatus', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getSessionStatus')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));
		
		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$waiting = $driver->ping($chatSessionModel, $session_data);

		$this->assertEquals($waiting, 2);
	}

	public function testPingOkOnline()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->State = 1;
		$_object->AgentNickName = 'Matias';
		$_object->isTyping = true;
		$_object->Text = ['12345687'];
		$_object->AgentId = 1000;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getSessionStatus', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getSessionStatus')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));

		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$online = $driver->ping($chatSessionModel, $session_data);

		// debe retornar los valores para enviar al chat
		$this->assertEquals($online['AgentNickName'], 'Matias');

		//Debe haberse iniciado la hora de chat
		$session_data = $chatSessionModel->getSessionData($session_data->token);
		$this->assertEquals($session_data->cc_start, date('Y-m-d H:i:s'));
	}

	public function testPingOkOnlineWithHistory()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->State = 1;
		$_object->AgentNickName = 'Matias';
		$_object->isTyping = true;
		$_object->Text = ['12345687'];
		$_object->AgentId = 1000;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getSessionStatus', 'getAppParam', 'postMessage'));
		// debe haberse llamado postMessage
		$mockEmergiaAPIClient->expects($this->exactly(1))->method('postMessage')->will($this->returnValue(true));
		$mockEmergiaAPIClient->expects($this->any())->method('getSessionStatus')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));

		$driver = new EmergiaDriver($mockEmergiaAPIClient);

		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => '12345', 'origin' => WatsonDriver::$bot_name);
		$chatSessionModel->updateChatHistory($session_data->token, $chat_record);

		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => '67890', 'origin' => 'blum');
		$chatSessionModel->updateChatHistory($session_data->token, $chat_record);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$session_data->send_chat = True;

		$driver->ping($chatSessionModel, $session_data);

		//Debe haberse puesto en False el flat send_chat
		$session_data = $chatSessionModel->getSessionData($session_data->token);
		$this->assertEquals($session_data->send_chat, False);
	}

	public function testPingOkKill()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->State = 3;

		$mockDriver = $this->getMockBuilder('\Drivers\EmergiaDriver')->setMethods(array('kill'))
                         ->disableOriginalConstructor()
                         ->getMock();
		$mockDriver->expects($this->exactly(1))->method('kill')->will($this->returnValue(1));

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getSessionStatus', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getSessionStatus')->will($this->returnValue(array('Code' => 0, 'Data' => $_object)));
		
		$mockDriver->api_client = $mockEmergiaAPIClient;

		$kill = $mockDriver->ping($chatSessionModel, $session_data);

		$this->assertEquals($kill, 3);
	}

	public function testPingEmergiaError()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getSessionStatus', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getSessionStatus')->will($this->returnValue(array('Code' => -20, 'ErrorMessage' => 'Error')));

		$driver =  new EmergiaDriver($mockEmergiaAPIClient);

		$this->setExpectedException(EmergiaError::class);
		$kill = $driver->ping($chatSessionModel, $session_data);

	}

	public function testPingEmptyResponse()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('getSessionStatus', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('getSessionStatus')->will($this->returnValue(null));
		$driver =  new EmergiaDriver($mockEmergiaAPIClient);
		$this->setExpectedException(EmptyTokenError::class);
		$kill = $driver->ping($chatSessionModel, $session_data);
		
	}

	public function testPostMessageOK()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('postMessage', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('postMessage')->will($this->returnValue(true));
		
		$driver =  new EmergiaDriver($mockEmergiaAPIClient);

		$response = $driver->postMessage($chatSessionModel, $session_data, 'blah', true);
		$this->assertEquals($response['fin_chat'], false);
	}

	public function testKill()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$mockEmergiaAPIClient = $this->getMock('ApiClient\EmergiaAPIClient', array('deleteChatSession', 'deleteToken', 'getAppParam'));
		$mockEmergiaAPIClient->expects($this->any())->method('deleteChatSession')->will($this->returnValue(1));
		$mockEmergiaAPIClient->expects($this->any())->method('deleteToken')->will($this->returnValue(1));
		
		$driver =  new EmergiaDriver($mockEmergiaAPIClient);

		$driver->kill($chatSessionModel, $session_data);

		$session_data = $chatSessionModel->getSessionData($session_data->token);
		$this->assertEquals($session_data->cc_end, date('Y-m-d H:i:s'));
	}

}