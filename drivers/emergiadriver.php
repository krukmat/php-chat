<?php

namespace Drivers;

// Definicion de Excepciones

class EmergiaError extends \Exception
{
}

class EmptyTokenError extends \Exception
{
}

class InvalidSession extends \Exception
{
}

class EmergiaDriver extends \Core\Driver
{
	public function __construct($api_client = null)
	{	if ($api_client != null)
			$this->api_client = $api_client;
		else
			// @codeCoverageIgnoreStart
			$this->api_client = new \ApiClient\EmergiaAPIClient();
			// @codeCoverageIgnoreEnd
	}

	private function getErrorMessage($message)
	{
		return $message['Code'].': '.$message['ErrorMessage'];
	}

	protected function getToken()
	{
		$token = $this->api_client->getToken();

		if (isset($token))
		{
			// Si Code === 0 => Token sino error
			if ($token['Code'] == 0)
				return $token['Data']->Token;
			else {
				throw new EmergiaError($this->getErrorMessage($token));
			}
		} else {
			$error = 'Empty Token: Try Later';
			throw new EmptyTokenError($error);
		}
	}

	protected function updateCredentials($token, $chat_session, $session_data, $priority, $session_creds)
	{
			$session_url = $this->api_client->getChatSession($token, $session_data->user_name, '', $session_data->user_email, $priority);

			// Chequeo que la sesion haya sido obtenida
			if (isset($session_url))
			{
				//session_token => token, chat_token => session_url
				$session_creds = array('emergia_session_token' => $token, 'emergia_chat_token' =>$session_url, 'priority' => $priority);
				$chat_session->updateSessionData($session_data->token, $session_creds);

				$response['response'] = array();
				$response['response']['ChatHistory'] = $session_data->chat_history;
				
				// Retorno el chat history en el caso de restore por navegar en otra pagina
				return $response;
			}
			$error = 'Invalid Session: Login again';
			throw new InvalidSession($error);
	}

	public function init($chat_session, $session_data, $priority = 1, $continue_session = false)
	{
		// Posibles estados: Sin Token, Error de API o token. En el caso de no token => Lanzar excepcion
		$response = array();
		$response['session_id'] = $session_data->token;

		//Si la sesion no se continua => se necesita un nuevo token.
		if ($continue_session == false)
		{
			$this->updateCredentials($this->getToken(), $chat_session, $session_data, $priority, $session_creds);
		} else {
			// Al restaurar la sesion solamente se devuelve el historial
			$response['response'] = array('ChatHistory' => $session_data->chat_history);
		}
		return $response;
	}

	// 3 estados: Waiting -> 2, Online-> 1, Offline -> 3+. Si es online => nombre del agente, texto y IsTyping
	public function ping($chat_session, $session_data)
	{
		$response = $this->api_client->getSessionStatus($session_data->emergia_session_token, $session_data->emergia_chat_token);
		//


		if (!isset($response))
		{
			$error = 'getSessionStatus: Empty response';
			throw new EmptyTokenError($error);
		}

		if ($response['Code'] == 0)
		{
			// Se ha iniciado sesion y quedo asignado el agente
			if ($response['Data']->State == 1)
			{	
				// La primera vez se actualiza el cc_start
				if (!isset($session_data->cc_start))
				{
					$session_creds = array('cc_start' => date('Y-m-d H:i:s'));
					$chat_session->updateSessionData($session_data->token, $session_creds);
				}

				// Post del historial completo al operador?
				if ($session_data->send_chat)
				{
					// mandar todo el historial como primer chat
        			$this->postCompleteChatHistory($chat_session, $session_data);
        			// Cambiar flag a false
        			$session_updates = array('send_chat' => False);
					$chat_session->updateSessionData($session_data->token, $session_updates);
				}

				// guardo en el historial si hay texto recibido
				$text = implode(' ', $response['Data']->Text);
				if ((isset($text)) && (strlen($text)>0))
				{

					//var_dump($response);
					$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => $text, 'origin' => $response['Data']->AgentNickName);
					$chat_session->updateChatHistory($session_data->token, $chat_record);
				}

				return array('AgentId' => $response['Data']->AgentId, 'AgentNickName' => $response['Data']->AgentNickName, 'IsTyping' => $response['Data']->IsTyping, 'Text'=> $text);
			}
			// Diferentes estados, pero en todos es cerrado
			if ($response['Data']->State > 2)
			{	
				$this->kill($chat_session, $session_data);
				return 3;
			}
			// Sino es el estado de espera
			return $response['Data']->State;
		} else {
			throw new EmergiaError($this->getErrorMessage($response));
		}
	}

	// TODO: $like no deberia estar. Enviar un objeto wrapper y dentro que este text.
	public function postMessage($chat_session, $session_data, $text, $like)
	{	
		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => $text, 'origin' => $session_data->user_email);
		$chat_session->updateChatHistory($session_data->token, $chat_record);
		$this->api_client->postMessage($session_data->emergia_session_token, $session_data->emergia_chat_token, $text);
		// por ahora el fin_chat lo dejo cableado en false aqui hasta que Emergia devuelva fin de chat desde el ping.
		$response = array();
		$response['fin_chat'] = false;
		return $response;
	}

	public function postCompleteChatHistory($chat_session, $session_data)
	{
		$chat_history = array();
		if (count($session_data->chat_history) > 0)
		{
			// Toda la historia se vuelca en un unico dialogo de chat
			$chat_history[] = '------Begin chat history-------';
			foreach ($session_data->chat_history as $chat_line)
			{
				// Si origin === Bot Watson => 
				if ($chat_line['origin'] == WatsonDriver::$bot_name)
				{
					$chat_history[] = 'Watson says '.$chat_line['text'].'<BR/>';
				} else {
					// Caso del usuario cliente
					$chat_history[] = $chat_line['origin'].' says '.$chat_line['text'].'<BR/>';
				}
				array_push($chat_history, $chat_line_text);
			}
			$chat_history[] = '------End chat history-------';
			$complete_history = implode(' ', $chat_history);
			// Se hace el post del historial completo al agente humano
			return $this->api_client->postMessage($session_data->emergia_session_token, $session_data->emergia_chat_token, $complete_history);
		}

		return null;
		
	}

	//@codeCoverageIgnoreStart
	public function putUserWriting($session_data)
	{
		return $this->api_client->putUserWriting($session_data->emergia_session_token, $session_data->emergia_chat_token);
	}

	public function deleteUserWriting($session_data)
	{
		return $this->api_client->deleteUserWriting($session_data->emergia_session_token, $session_data->emergia_chat_token);
	}
	//@codeCoverageIgnoreEnd

	public function kill($chat_session, $session_data)
	{	// elimino token y sesion de chat
		// Si 0: Tira error de maxima execution
		$this->api_client->deleteChatSession($session_data->emergia_session_token, $session_data->emergia_chat_token, 1);
		$this->api_client->deleteToken($session_data->emergia_session_token);
		// Actualizo el estado de cierre
		$session_creds = array('cc_end' => date('Y-m-d H:i:s'));
		$chat_session->updateSessionData($session_data->token, $session_creds);
	}

}