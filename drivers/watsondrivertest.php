<?php
namespace Drivers;


require_once dirname(__FILE__) .'/../core/model.php';
require_once dirname(__FILE__) .'/../core/entity.php';
require_once dirname(__FILE__) .'/../core/modeltest.php';
require_once dirname(__FILE__) .'/../core/driver.php';

require_once dirname(__FILE__) .'/../apiclient/oauthclient.php';
require_once dirname(__FILE__) .'/../apiclient/watsonapiclient.php';
require_once dirname(__FILE__) .'/../model/chatsessionmodel.php';
require_once dirname(__FILE__) .'/../entities/chatsessionentity.php';
require_once dirname(__FILE__) .'/watsondriver.php';

/**
 * Description of WatsonDriver Tests
 *
 * @author krukmat
 */
 
class WatsonDriverTest extends \Model\ModelTest
{
	protected function setUp()
    {
        global $chatSessionModel, $modelName, $session_data;
        
		$chatSessionModel = new \Model\ChatSessionModel();
		$modelName = $chatSessionModel->getModelName();  //It's used for delete the collection aftwerwards
		\Model\ModelTest::setUp();
		$entity = $chatSessionModel->createChatSession();
		$session_id = $entity->token;
		$session_data = $chatSessionModel->getSessionData($session_id);
    }

	public function testInitOk()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->output = new \stdClass();
		$_object->output->text = array('text');
		$_object->conversation_id = 'something';

		$_token_object = new \stdClass();
		$_token_object->token = '123456';

		$mockWatsonAPIClient = $this->getMock('ApiClient\WatsonAPIClient', array('init', 'getToken', 'getAppParam'));
		$mockWatsonAPIClient->expects($this->exactly(1))->method('init')->will($this->returnValue($_object));
		$mockWatsonAPIClient->expects($this->exactly(1))->method('getToken')->will($this->returnValue($_token_object));

		$driver = new WatsonDriver($mockWatsonAPIClient);

		$response = $driver->init($chatSessionModel, $session_data);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$this->assertEquals($session_data->ia_start, date('Y-m-d H:i:s'));
		$this->assertEquals($session_data->watson_conversation_id, "something");
		$this->assertEquals(count($session_data->chat_history), 1);
	}

	public function testInitEmptyToken()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->output = new \stdClass();
		$_object->output->text = 'text';
		$_object->conversation_id = 'something';

		$_token_object = new \stdClass();
		$_token_object->token = '123456';

		$mockWatsonAPIClient = $this->getMock('ApiClient\WatsonAPIClient', array('init', 'getToken', 'getAppParam'));
		$mockWatsonAPIClient->expects($this->exactly(1))->method('getToken')->will($this->returnValue(null));

		$driver = new WatsonDriver($mockWatsonAPIClient);
		
		$this->setExpectedException(\APIClient\WatsonError::class);

		$response = $driver->init($chatSessionModel, $session_data);

	}

	public function testInitContinueTrue()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->output = new \stdClass();
		$_object->output->text = 'text';
		$_object->conversation_id = 'something';

		$_token_object = new \stdClass();
		$_token_object->token = '123456';

		$mockWatsonAPIClient = $this->getMock('ApiClient\WatsonAPIClient', array('init', 'getToken', 'getAppParam'));

		$driver = new WatsonDriver($mockWatsonAPIClient);

		$session_data->chat_history = array(1,2,3,4,5,6,7,8,9,10);

		$response = $driver->init($chatSessionModel, $session_data, 1, true);

		$this->assertEquals($response['response']['AgentId'], '666');
		$this->assertEquals($response['response']['AgentNickName'], 'Aida');
		$this->assertEquals($response['response']['ChatHistory'], $session_data->chat_history);

	}

	public function testPostMessageOk()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->output = new \stdClass();
		$_object->output->text = 'text';
		$_object->context = new \stdClass();
		$_object->context->cont = 1;
		$_object->fin_chat = false;

		$mockWatsonAPIClient = $this->getMock('ApiClient\WatsonAPIClient', array('chat', 'getAppParam'));
		$mockWatsonAPIClient->expects($this->any())->method('chat')->will($this->returnValue($_object));

		$driver = new WatsonDriver($mockWatsonAPIClient);

		$response = $driver->postMessage($chatSessionModel, $session_data, 'hey there', true);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$this->assertEquals(count($session_data->chat_history), 2);
		$this->assertEquals($response['redirect_emergia'], false);
		$this->assertEquals($response['fin_chat'], false);
	}

	public function testPostMessageRedirectEmergia()
	{	
		global $chatSessionModel, $modelName, $session_data;

		$_object = new \stdClass();
		$_object->output = new \stdClass();
		$_object->output->text = 'text';
		$_object->conversation_id = '12345';
		$_object->redirigir = true;
		$_object->fin_chat = true;

		$mockWatsonAPIClient = $this->getMock('ApiClient\WatsonAPIClient', array('chat', 'getAppParam'));
		$mockWatsonAPIClient->expects($this->any())->method('chat')->will($this->returnValue($_object));

		$driver = new WatsonDriver($mockWatsonAPIClient);

		$response = $driver->postMessage($chatSessionModel, $session_data, 'hey there', true);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$this->assertEquals(count($session_data->chat_history), 2);
		$this->assertEquals($response['redirect_emergia'], true);
		$this->assertEquals($response['fin_chat'], false);
	}

	public function testKill()
	{
		global $chatSessionModel, $modelName, $session_data;

		$mockWatsonAPIClient = $this->getMock('ApiClient\WatsonAPIClient', array('chat', 'getAppParam'));
		$mockWatsonAPIClient->expects($this->any())->method('chat')->will($this->returnValue($_object));

		$driver = new WatsonDriver($mockWatsonAPIClient);

		$response = $driver->kill($chatSessionModel, $session_data);

		$session_data = $chatSessionModel->getSessionData($session_data->token);

		$this->assertEquals($session_data->ia_end, date('Y-m-d H:i:s'));
	}
}