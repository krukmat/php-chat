<?php

namespace Drivers;

class WatsonDriver extends \Core\Driver
{
	static $bot_name = 'Aida';
	static $bot_id = '666';

	public function __construct($api_client = null)
	{
		if ($api_client != null)
			$this->api_client = $api_client;
		else
			// @codeCoverageIgnoreStart
			$this->api_client = new \ApiClient\WatsonAPIClient();
			// @codeCoverageIgnoreEnd
		$this->bot_name = WatsonDriver::$bot_name;
		$this->bot_id = WatsonDriver::$bot_id;
	}

	// El texto retornado en el caso de Watson es un array o no. Chequear que es y retornarlo plano.
	private function getTextResponse($text)
	{
		if (is_array($text))
		{
			return implode(' ', $text);
		}
		return $text;
	}

	private function createAgentResponse()
	{
		return array('AgentId' => $this->bot_id, 'AgentNickName' => $this->bot_name, 'IsTyping' => false);
	}

	public function init($chat_session, $session_data, $priority = 1, $continue_session=false)
	{
		$response = array();
		$response['session_id'] = $session_data->token;
		
		if ($continue_session == false) 
		{
				// oauth token
				$token = $this->api_client->getToken('post');
				if (isset($token))
				{
					$token = $token->token;
					// guardar token
					$api_response = $this->api_client->init($token, $session_data->user_name);
					// inicio de chat con watson. debido a que devuelve un array se hace un implode.
					$text = $this->getTextResponse($api_response->output->text);
					//Actualizar token y sesion de chat
					$this->updateInitCredentials($token, $chat_session, $session_data, $api_response);
					// texto de respuesta para el layout
					$this->updateAgentChatHistory($chat_session, $session_data, $text);
					$response['response'] = $this->createAgentResponse();
					$response['response']['Text'] = $text;
					$response['response']['ChatHistory'] = $session_data->chat_history;
				} else {
					throw new \APIClient\WatsonError('Token indefinido');
				}
		} else {
			// Al restaurar la sesion solamente se devuelve el historial
			$response['response'] = $this->createAgentResponse();
			$response['response']['Text'] = '';
			$response['response']['ChatHistory'] = $session_data->chat_history;
		} 
		return $response;
	}

	// @codeCoverageIgnoreStart
	public function ping($chat_session, $session_data)
	{
		$response = array();
		$response['ChatHistory'] = $session_data->chat_history;
		return $response;
	}
	// @codeCoverageIgnoreEnd

	private function updateUserChatHistory($chat_session, $session_data, $text, $like)
	{
		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => $text, 'origin' => $session_data->user_email, 'like' => $like);
		$chat_session->updateChatHistory($session_data->token, $chat_record);
	}

	private function updateAgentChatHistory($chat_session, $session_data, $text)
	{
		$chat_record = array('date' => date('Y-m-d H:i:s'), 'text' => $text, 'origin' => $this->bot_name);
		$chat_session->updateChatHistory($session_data->token, $chat_record);
	}

	private function updateInitCredentials($token, $chat_session, $session_data, $api_response)
	{
		$watson_data = array('watson_token' => $token, 'watson_conversation_id' => $api_response->conversation_id, 'success_ia' => true);
		if (!isset($session_data->ia_start))
		{
			$watson_data['ia_start'] = date('Y-m-d H:i:s');
		}
		$chat_session->updateSessionData($session_data->token, $watson_data);
	}

	private function updateCredentials($chat_session, $session_data, $api_response)
	{
		$watson_data = array('watson_conversation_id' => $api_response->conversation_id, 'success_ia' => true);
		// si ia_start no esta definido => se setea fecha-hora de inicio.
		if (!isset($session_data->ia_start))
		{
			$watson_data['ia_start'] = date('Y-m-d H:i:s');
		}
		$chat_session->updateSessionData($session_data->token, $watson_data);
	}

	public function postMessage($chat_session, $session_data, $text, $like)
	{
		$this->updateUserChatHistory($chat_session, $session_data, $text, $like);

		// post a api de watson
		$api_response = $this->api_client->chat($session_data->watson_token, $like, $text, $session_data->watson_conversation_id);

		// actualizo context. success_ia: flag que define que el api devolvio un resultado sin error.
		$this->updateCredentials($chat_session, $session_data, $api_response);
		// response de watson. redirigir: define si se debe redirigir a operador humano.
		$fin_chat = $api_response->fin_chat;
		$response = array('response' => $this->createAgentResponse());
		// En el caso de que Watson no pueda responder => redirect_emergia = true. Y comienza el circuito normal de atencion con maxima prioridad.
		if  ($api_response->redirigir)
		{
			// En el caso de redireccion enviar el texto definido en configuracion
			$response['response']['Text'] = \Model\ConfigParamsModel::getMessageAgentDerivation();
			$response['redirect_emergia'] = true; 
			$response['fin_chat'] = false;
		}else{
			// actualizar registros de chat del bot solo si aun no se llego al umbral para redirigir a Emergia. En ese caso la respuesta se descarta y se pasa a Emergia
			// Eliminacion del array que envia Watson. Pasa todo a string.
			$response['response']['Text'] = $this->getTextResponse($api_response->output->text); ;
			$response['redirect_emergia'] = false; 
			$response['fin_chat'] = $fin_chat;
		}
		// Valores con la respuesta dada al cliente. 
		$this->updateAgentChatHistory($chat_session, $session_data, $response['response']['Text']);
		return $response;
	}


	public function kill($chat_session, $session_data)
	{	
		// Actualizo el estado de cierre
		$session_creds = array('ia_end' => date('Y-m-d H:i:s'));
		$chat_session->updateSessionData($session_data->token, $session_creds);
	}

	// @codeCoverageIgnoreStart
	public function putUserWriting($session_data)
	{
		return true;
	}

	public function deleteUserWriting($session_data)
	{
		return true;
	}
	// @codeCoverageIgnoreEnd


}