<?php 

error_reporting(E_ALL);
ini_set("display_errors", "on");

require_once("core".DIRECTORY_SEPARATOR."application.php");
$application = new \Core\Application(false);

$request = new \Core\RequestController();
$request->execute();