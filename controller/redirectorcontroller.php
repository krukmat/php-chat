<?php
namespace Controller;
include('apiclient/lib/httpful.phar');

class RedirectorController extends \Core\BaseController
{	

	protected function switchToEmergia($chat_session, $session_data, $session_config, $priority=50)
	{	
		// se termina la sesion con Watson.
		$this->driver->kill($chat_session, $session_data);
		// success_ia en false
                $chat_session->updateSessionData($session_data->token, $session_config);
                // inicializar driver de emergia
                $session_data = $chat_session->getSessionData($session_data->token);
                // Definir que se envie el historial de chat ni bien este disponible el agente
                $session_updates = array('send_chat' => True);
                $chat_session->updateSessionData($session_data->token, $session_updates);
                // driver a emergia
                $this->setDriver();
                $response = $this->driver->init($chat_session, $session_data, $priority, false);
                // enviar redirect_emergia
                $response['redirect_emergia'] = true;
                return $response;
	}

}
