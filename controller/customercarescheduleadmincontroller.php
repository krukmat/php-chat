<?php

namespace Controller;

/**
 * Description of CustomerCareScheduleAdminController
 *
 * @author krukmat
 */
 
class CustomerCareScheduleAdminController extends \Core\BaseController
{

    /**
     *     
     *  It returns the complete list of time tables
     *  @author: krukmat
    */

    function GET() 
    {
        $customerCareScheduleModel = new \Model\CustomerCareScheduleModel();

        if ($customerCareScheduleModel == null) {
            $this->error404();
        }

        $only_standard = $this->defaultGETValue('only_standard', null);

        if ($only_standard=== null)
        {
            $date_start = $this->defaultGETValue('date_start', null);
            $date_end = $this->defaultGETValue('date_end', null);
            // deben estar definidas las 2 fechas
            if ((isset($date_start)) && (isset($date_end)))
            {
                $date_start = new \DateTime(date('Y-m-d H:i', strtotime($date_start)));
                $date_end = new \DateTime(date('Y-m-d H:i', strtotime($date_end)));

                // return the complete list of service time table available
                $result = $customerCareScheduleModel->getScheduleTable($date_start, $date_end);   
            } else {
                $this->error404();
            }

            
        } else {
             $result = $customerCareScheduleModel->getStandardScheduleTable(); 
        }

        $view = new \Core\RenderView("templates\json", "json");
        $view->assign("data", $result);
    }


    /**
     *     
     *  It loads a record with dates-days-hours available for the service
     *  @author: krukmat
    */

    function POST() 
    {
        $date_from = null;
        $date_to = null;

        $customerCareScheduleModel = new \Model\CustomerCareScheduleModel();

        // TODO: Esto en un metodo de modelo. sacar de aca...

        $only_standard = $this->defaultGETValue('only_standard', null);

        $week_days = [];

        if (isset($_POST['week_days']) && (count($_POST['week_days'])>0) )
        {
            foreach ($_POST['week_days']  as  $value) 
            {
                $week_days[] = intval($value);
            }
        }

        if ($only_standard) 
        {
            // si es standard primero elimino el valor. 
            $filter = array("week_days" => $week_days);
            $result = $customerCareScheduleModel->delete($filter);
        }

        if (($this->defaultPOSTValue('date_from', false)) && ($this->defaultPOSTValue('date_to', false)))
        {
            // Hay valores dentro de date_from y date_to => chequeo que sean fechas validas
            $date_from_time = strtotime($this->defaultPOSTValue('date_from', "not valid"));
            $date_to_time = strtotime($this->defaultPOSTValue('date_to', "not valid"));

            if ($date_from_time)
                $date_from = date('Y-m-d', $date_from_time);
            else {
                // Es una fecha invalida
                $this->error404();
            }
            
            if ($date_to_time)
                $date_to = date('Y-m-d', $date_to_time);
            else {
                // Es una fecha invalida
                $this->error404();
            }
        }


        $time_noon_from = $this->defaultPOSTValue('time_noon_from', null);
        $time_noon_to = $this->defaultPOSTValue('time_noon_to', null);

        //ambos indefinidos => chequeo el par night
        if ((!$time_noon_from) && (!$time_noon_to))
        {
            $time_night_from = $this->defaultPOSTValue('time_night_from', null);
            $time_night_to = $this->defaultPOSTValue('time_night_to', null);

            if ((!$time_night_from) && (!$time_night_to)) 
            {
                // no se han definido al menos un par de from to
                $this->error404();
            }

        }
        
        //var_dump(get_defined_vars()); 

        $customerCareScheduleModel = new \Model\CustomerCareScheduleModel();
        if ($customerCareScheduleModel == null) {
            $this->error404();
        }

        $token = $this->defaultPOSTValue('token', null);

        if (isset($token))
            $result = $customerCareScheduleModel->updateCustomerCaresSchedule($token, $date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);
        else
            $result = $customerCareScheduleModel->createCustomerCaresSchedule($date_from, $date_to, $week_days, $time_noon_from, $time_noon_to, $time_night_from, $time_night_to);

        if ($result == null) {
            $this->error404();
        }

        if (isset($result->date_from))
            $result->date_from = $result->readableDateFrom();
        if (isset($result->date_to))
            $result->date_to = $result->readableDateTo();

        $view = new \Core\RenderView("templates\json", "json");
        $view->assign("data", $result);
    }

    function DELETE()
    {    
        $token = $this->defaultGETValue('token', null);

        if (!$token)
            $this->error404();

        $filter = array("token" => $token);

        $customerCareScheduleModel = new \Model\CustomerCareScheduleModel();
        $result = $customerCareScheduleModel->delete($filter);

        $this->success200();

    }

}