<?php
namespace Controller;
/**
 * Description of ChatApiController
 *
 * @author krukmat
 */
class ChatApiController extends RedirectorController
{	
	public function __construct()
	{
		$this->chat_session = new \Model\ChatSessionModel();
	}

    function GET() 
    {	
    	// ping
    	$session_data = $this->validateSession(false);
		try 
		{
			$response = $this->driver->ping($this->chat_session, $session_data);
	    }
	    catch (Exception $e) 
		{
			$response = array('error' => $e->getMessage());
		}
		catch(\Drivers\EmergiaError $e)
		{	
			$response = array('error' => $e->getMessage());
		}
		
		$view = new \Core\RenderView("templates\json", "json");
    	$view->assign("data", $response);
		$this->success200();
	}

	function POST()
	{
		$session_data = $this->validateSession(false);
		$text = $_POST['text'];
		$like = $this->defaultPOSTValue('like', false);

		try 
		{
		    $response = $this->driver->postMessage($this->chat_session, $session_data, $text, $like);
		    // en el caso de Watson chequear si redirect_emergia es true => se debe pasar a operador humano
		    if ((isset($response)) && ($response['redirect_emergia'] == true))
		    {
		    	throw new \APIClient\WatsonError('Changing to Emergia');
		    }
		} 
		catch (\APIClient\WatsonError $e) 
		{
			// Matar sesion en Watson. Y pasar a Emergia con prioridad maxima.
			$session_config = array('success_ia' => false);
			$this->switchToEmergia($this->chat_session, $session_data, $session_config);  
		}

		$view = new \Core\RenderView("templates\json", "json");
        $view->assign("data", $response);

		$this->success200();
	}

	function PUT()
	{
		$session_data = $this->validateSession(false);
		$this->driver->putUserWriting($session_data);
		$this->success200();
	}

	function DELETE()
    {	

    	$session_data = $this->validateSession();

		if ($this->operation == 'userwriting')
		{
			if ($this->driver->deleteUserWriting($session_data))
				$this->success200();
			else
				$this->error404();
		}

		if ($this->operation == 'kill')
		{
			$this->driver->kill($this->chat_session, $session_data);
			$this->success200();
		}

		$this->error404();
    }

}