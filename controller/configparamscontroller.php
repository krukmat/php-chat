<?php
namespace Controller;
/**
 * Description of ConfigController
 *
 * @author krukmat
 */
class ConfigParamsController extends \Core\BaseController
{	
	public function __construct()
	{
		$this->model = \Model\ConfigParamsModel::singleton();
	}

	function GET()
	{	
		$entity = $this->model->current();
		$view = new \Core\RenderView("templates\json", "json");
        $view->assign("data", $entity);
	}

	function POST()
    {	
    	// Como config es un solo registro, se borra el existente y se reemplaza por el nuevo contenido.

    	$watson_rate = $_POST['watson_rate'];
    	$message_agent_derivation = $_POST['message_agent_derivation'];
    	$message_out_of_line = $_POST['message_out_of_line'];

    	$this->model->save($watson_rate, $message_agent_derivation, $message_out_of_line);
    	
    	$this->success200();
    }
}