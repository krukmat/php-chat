<?php

namespace Controller;

/**
 * Description of SurveyController
 *
 * @author krukmat
 */
class SurveyController extends \Core\BaseController
{


    public function __construct()
    {
        $this->chat_session = new \Model\ChatSessionModel();
    }

    /**
     * 
     */
    function POST()
    {
        $session_id = $_POST['session_id'];    
        $session_data = $this->chat_session->getSessionData($session_id);

        if  (!isset($session_data))
            $this->error404();

        $survey_mm = $_POST['survey_mm'];
        $survey_chat = $_POST['survey_chat'];
        $survey_comments = $_POST['survey_comments'];

        $survey_data = array('survey_mm' => $survey_mm,'survey_chat' =>$survey_chat, 'survey_comments' => $survey_comments);
        $this->chat_session->updateSessionData($session_data->token, $survey_data);
        $this->success200();
    }

}