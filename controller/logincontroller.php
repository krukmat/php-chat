<?php

namespace Controller;

/**
 * Description of LoginController
 *
 * @author krukmat
 */
class LoginController extends RedirectorController
{   

    function GET()
    {   
        // init => restablecimiento de la sesion
        $customerCareScheduleModel = new \Model\CustomerCareScheduleModel();
        $current_date = date('Y-m-d H:i');
        $is_available = $customerCareScheduleModel->isServiceAvailable($current_date);
        $response = array();
        if ($is_available)
        {
            $this->chat_session = new \Model\ChatSessionModel();
            $session_data = $this->validateSession(false);
            try 
            {
                $response = $this->driver->init($this->chat_session, $session_data, 0, true);
            }
            catch (Exception $e) 
            {
                $response = array('error' => $e->getMessage());
            }
            $response['send_to_ia'] = ($session_data->send_to_ia && !(isset($session_data->ia_end)));
        } else {
            $response['show_contact_form_message'] = \Model\ConfigParamsModel::getMessageOutOfLine();
        }
        $response['show_contact_form'] = !$is_available; // El inverso de la logica muestra el formulario de contacto
        $view = new \Core\RenderView("templates\json", "json");
        $view->assign("data", $response);
        $this->success200();
    }

    function POST()
    {
        $user_name = $_POST['user_name'];
        $user_email = $_POST['user_email'];

        $chatSessionModel = new \Model\ChatSessionModel();
        // user_agent, ip, language
        $headers = array('http_user_agent' => $_SERVER['HTTP_USER_AGENT'], 'remote_addr' => $_SERVER['REMOTE_ADDR'], 'http_x_forwarded_for' => $_SERVER['HTTP_X_FORWARDED_FOR'], 'http_client_ip' => $_SERVER['HTTP_CLIENT_IP'], 'http_accept_language' => $_SERVER['HTTP_ACCEPT_LANGUAGE']);

        $chat_data = $chatSessionModel->createChatSession($user_name, $user_email, $headers);

        if ($chat_data == null) 
        {
            $this->error404();
        } else {
            // Creo el enlace con el driver correct segun send_to_ia.
            // logueo con el api
            if (!$chat_data->send_to_ia)
                $this->setDriver();
            else
                $this->setDriver('watson');
            try 
            {
                $response = $this->driver->init($chatSessionModel, $chat_data);
                $response['send_to_ia'] = $chat_data->send_to_ia;
                $view = new \Core\RenderView("templates\json", "json");
                $view->assign("data", $response);
                $this->success200();
            }
            catch (\APIClient\WatsonError $e) 
            {   
                // Si tira error al tratar de iniciar comunicacion con watson => cancelo y continúo con emergia.
                $session_config = array('send_to_ia' => false, 'success_ia' => false);
                $session_data = $chatSessionModel->getSessionData($chat_data->token);
                //flag send_to_ia a falso
                $response = $this->switchToEmergia($chatSessionModel, $session_data, $session_config, 1);  
                $response['send_to_ia'] = false;
                $view = new \Core\RenderView("templates\json", "json");
                $view->assign("data", $response);
                $this->success200();
            } 
        }
    }

}
