<?php

namespace Controller;

/**
 * Description of CustomerCareScheduleController
 *
 * @author krukmat
 */
 
class CustomerCareScheduleController extends \Core\BaseController
{
	function defaultPOSTValue($var, $default){
		if (isset($_POST[$var])) {
			return $_POST[$var];
		}
		return $default;
	}

    function GET() 
    {
        $customerCareScheduleModel = new \Model\CustomerCareScheduleModel();

        if ($customerCareScheduleModel == null) {
            $this->error404();
        }

        $current_date = date('Y-m-d H:i');

        $is_available = !$customerCareScheduleModel->isServiceAvailable($current_date);
        
        $result = array(
            "show_contact_form" => $is_available,
            "show_contact_form_message" => \Model\ConfigParamsModel::getMessageOutOfLine()
        );

        $view = new \Core\RenderView("templates\json", "json");
        $view->assign("data", $result);
    }
		
}
