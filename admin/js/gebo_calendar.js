/* [ ---- Gebo Admin Panel - calendar ---- ] */

	$(document).ready(function() {
		gebo_calendar.regular();
		//* resize elements on window resize
		var lastWindowHeight = $(window).height();
		var lastWindowWidth = $(window).width();
		$(window).on("debouncedresize",function() {
			if($(window).height()!=lastWindowHeight || $(window).width()!=lastWindowWidth){
				lastWindowHeight = $(window).height();
				lastWindowWidth = $(window).width();
				//* rebuild calendar
				$('#calendar').fullCalendar('render');
			}
		});
	});
	
	//* calendar
	gebo_calendar = {
		regular: function() {
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			$.get('/customercarescheduleadmin?date_start='+parseInt(y)+'-01-01&date_end='+parseInt(y)+'-12-31').done(function(data){
					let events = [];
					// Por cada dia => {time_night: {time_night_from,time_night_to}, time_noon: {time_noon_from,time_noon_to}}
					// Si hay array => creo un nuevo start, end sino no. 
					for(element in data) {
						if (data[element]['time_night'] !== null)
							events.push({token: data[element]['token'], 
								title:'Horario de atencion', 
								start: new Date(moment(data[element]['time_night']['time_night_from'])), 
								allDay: false, 
								end: new Date(moment(data[element]['time_night']['time_night_to'])),
								editable: true,
								startEditable: true,
								endEditable: true
							});
					if (data[element]['time_noon'] !== null)
							events.push({token: data[element]['token'], title:'Horario de atencion', 
								start: new Date(moment(data[element]['time_noon']['time_noon_from'])), 
								allDay: false, 
								end: new Date(moment(data[element]['time_noon']['time_noon_to'])),
								editable: true,
								endEditable: true
							});
					}
					var calendar = $('#calendar').fullCalendar({
						header: {
							left: 'prev next',
							center: 'title,today',
							right: 'month'
						},
						buttonText: {
							prev: '<i class="icon-chevron-left cal_prev" />',
							next: '<i class="icon-chevron-right cal_next" />'
						},
						aspectRatio: 2,
						selectable: false,
						selectHelper: true,
						disableDragging: true,
						allDayDefault: false,
						eventClick: function(calEvent, jsEvent, view) {
					        let time_noon_from = new Date(moment(calEvent.start));
							let time_noon_to = new Date(moment(calEvent.end));
							calendarTimeUpdateSettingModal($('#calendar'), calEvent.token, date, 
								time_noon_from, time_noon_to, calEvent)
					    },
						dayClick: function(date, jsEvent, view, resourceObj) {
						        calendarTimeCreateSettingModal($('#calendar'), date);
						       	
						},
						editable: true,
						theme: false,
						events: events,
						eventColor: '#bcdeee'
				})
			}).error(function(data){
                    console.log(data);
                    window.alert(data.status+': '+data.statusText);
			})
		}
	};