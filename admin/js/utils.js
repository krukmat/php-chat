function formatDate(d)
{
	var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();
    return curr_year + '-' + curr_month + '-' + curr_date;
}

function formatTime(time){
	return ("0" + time.getHours() ).slice(-2) + ':' + ("0"  + time.getMinutes() ).slice(-2);
}

function createModal(){
	return $('#hour_dialog').clone();
}

function calendarTimeUpdateSettingModal(calendar, token, date, time_noon_from, time_noon_to, _event){
		let modal_create = createModal();
		
		$(modal_create).on('show.bs.modal', function (event) {
			  let modal = $(this);
			  let save_button = $(modal).find('#save_date')[0];
			  let delete_button = $(modal).find('#delete')[0];

			  // set times from and to
			  let picker = $(modal).find('.timepicker-time').timepicker({
		                        timeFormat: 'H:i',
		                        step: 60
		                    });
			  let time_noon_from_timepicker = $(modal.find('#time_noon_from'));
			  let time_noon_to_timepicker = $(modal.find('#time_noon_to'));
			  time_noon_from_timepicker.timepicker('setTime', time_noon_from);
			  time_noon_to_timepicker.timepicker('setTime', time_noon_to);

			  //delete button
			  $(delete_button).click( function() {
					$.ajax({
					    url: "/customercarescheduleadmin?token=" + token,
					    type: 'DELETE'
					}).done( function(data) {
	            		//TODO: Control de errores
	            		calendar.fullCalendar('removeEvents',
							_event._id
						);
						//elimino el modal
						$(modal_create).remove();
	        		} );
			  });

			  //save button
			  $(save_button).click( function() {
			  		$(modal).modal('hide');
			  		let time_noon_from_value = $(modal.find('#time_noon_from')).val();
					let time_noon_to_value = $(modal.find('#time_noon_to')).val();
					// POST al server y creacion del nuevo start-end:time_from-time_to
					let http_object = {};
					http_object['date_from'] = formatDate(time_noon_from);
					http_object['date_to'] = formatDate(time_noon_from);
					http_object['time_noon_from'] = time_noon_from_value;
					http_object['time_noon_to'] = time_noon_to_value;
					http_object['token'] = token;
					$.post( "/customercarescheduleadmin", http_object).done( function(data) {
	            		//TODO: Control de errores
	            		let new_start = moment(data['date_from']+' '+data['time_noon_from']).toDate();
	            		let new_end = moment(data['date_from']+' '+data['time_noon_to']).toDate();
	            		_event.start = new_start;
	            		_event.end = new_end;
	            		calendar.fullCalendar( 'updateEvent', _event );
						// Borrar el evento anterior y recrearlo?
						$(modal_create).remove();
	        		} );
				});
		});
		$(modal_create).modal('show');
		calendar.fullCalendar('unselect');
	}

function calendarTimeCreateSettingModal(calendar, date){
		let modal_create = createModal();
		var inserted_data;
		
		$(modal_create).on('show.bs.modal', function (event) {
		
		      let modal = $(this);
			  let picker = $(modal).find('.timepicker-time').timepicker({
		                        timeFormat: 'H:i',
		                        step: 60
		                    });
		      let now = new Date();
			  $(picker).timepicker('setTime', new Date(now.getFullYear(), now.getMonth(), now.getDate(), 10, 0, 0));

			  let save_button = $(modal).find('#save_date')[0];
			  $(save_button).click( function() {
			  		$(modal).modal('hide');
			  		let time_noon_from = $(modal.find('#time_noon_from')).val();
					let time_noon_to = $(modal.find('#time_noon_to')).val();
					// POST al server y creacion del nuevo start-end:time_from-time_to
					let http_object = {};
					http_object['date_from'] = formatDate(date);
					http_object['date_to'] = formatDate(date);
					http_object['time_noon_from'] = time_noon_from;
					http_object['time_noon_to'] = time_noon_to;

					$.post( "/customercarescheduleadmin", http_object).done( function(data) {
	            		inserted_data = data;
	            		//TODO: Control de errores
	            		calendar.fullCalendar('renderEvent',
							{	title: 'Horario de atencion',
								start: new Date(moment(formatDate(date)+' '+time_noon_from)),
								end: new Date(moment(formatDate(date)+' '+time_noon_to)),
								token: inserted_data['token'],
								allDay: false
							},
							true
						);
						$(modal_create).remove();
	        		} );

					
				});
		});
		// El boton delete para el formulario de modificar
		$($(modal_create).find('#delete')).hide();
		$(modal_create).modal('show');
		calendar.fullCalendar('unselect');
	}